var loading = "<img src='" + app.root + "images/loader.gif' />",
  loadingbgiconct =
    ' <div class="loadingbg"><div class="loadingbgicon"></div></div>',
  loadingcomment = '<div class="loadingcomment"></div>',
  MSGBOXTIME = 4000,
  REDIRECTTIME = 5000,
  TIMEOUTVALUE = 180000,
  mobileCounter,
  body = $("body");
if ("serviceWorker" in navigator) {
  navigator.serviceWorker.register("/sw.js");
}
app.map = body.attr("data-map");
function ajaxError(b) {
  if (b === 0) {
    return "خطأ في الأتصال تأكد من خدمة الأنترنت";
  } else {
    if (b === 404) {
      return "خطأ .. صفحة غير موجودة";
    } else {
      if (b === 500) {
        return "عذرا .. لا يمكن تنفيذ طلبك حاليا";
      } else {
        if (b === "parsererror") {
          return "عذرا .. لا يمكن تنفيذ طلبك حاليا";
        } else {
          if (b === "timeout") {
            return "عذرا ... الوقت المسموح بتفيذ طلبك أنتهي أعد المحاولة مرة آخري";
          } else {
            if (b === "abort") {
              return "تم إلغاء هذا الأتصال";
            } else {
              return "عذرا ... هناك خطأ أعد المحاولة مرة آخري";
            }
          }
        }
      }
    }
  }
}
function callAjx(e) {
  var f = $("#" + e.sendBtn);
  if (e.msgalert === false) {
    if (!$("#" + e.resdiv).length) {
      $("#" + e.containerid).after("<div  id='" + e.resdiv + "'></div>");
    }
    var g = $("#" + e.resdiv);
    g.empty();
  }
  if (e.loadingicon === false) {
    var h = f.val();
    f.val("جاري الارسال ...");
  } else {
    if ($("#" + e.loadingicon).length === 0) {
      f.after(
        "&nbsp;<img  id='" +
          e.loadingicon +
          "' class='loadingajax' src='" +
          app.root +
          "images/loader.gif' alt='جاري التحميل..' title='جاري التحميل..' />"
      );
    }
    hideshow(e.loadingicon, 1);
  }
  disableEnable(e.sendBtn, 1);
  $.ajax({
    type: e.sendtype,
    url: app.root + e.fullurl + "?lang=" + app.lang,
    cache: e.cached,
    data: $("#" + e.formid).serialize(),
    dataType: e.datetype,
    timeout: TIMEOUTVALUE,
    tryCount: 0,
    retryLimit: 3,
    success: function (a) {
      if (e.datetype === "html") {
        $("#" + e.containerid).html(a);
      } else {
        if (parseInt(a.status) === 1) {
          if (e.calltype !== false) {
            callbackSuccess(a, e.calltype);
          }
          if (e.incontainer === true) {
            $("#" + e.containerid).html(getMessage("success", false, a.txt));
            if (e.msgalert === false) {
              g.remove();
            }
          } else {
            if (e.msgalert === false) {
              g.html(getMessage("success", false, a.txt));
            } else {
              Msg.success(a.txt, MSGBOXTIME);
            }
            if (e.loadingicon === false) {
              f.val(h);
            } else {
              hideshow(e.loadingicon, 0);
            }
            if (e.disablesuccessBtn) {
              if (
                e.disablesuccessBtntxt !== null &&
                e.disablesuccessBtntxt !== ""
              ) {
                f.val(e.disablesuccessBtntxt);
              }
              disableEnable(e.sendBtn, 1);
            } else {
              disableEnable(e.sendBtn, 0);
            }
          }
          if (e.redirect === true) {
            setTimeout(function () {
              $(location).attr("href", e.redirecturl);
            }, REDIRECTTIME);
          }
          if (e.modal === true) {
            $("#modal-footer").remove();
          }
        } else {
          if (parseInt(a.status) === 0 || parseInt(a.status) === 2) {
            if (e.msgalert === false) {
              g.html(getMessage("danger", false, a.txt));
            } else {
              if (parseInt(a.status) === 2) {
                callbackError(a.txt);
              } else {
                Msg.danger(a.txt, MSGBOXTIME);
              }
            }
            if (e.loadingicon === false) {
              f.val(h);
            } else {
              hideshow(e.loadingicon, 0);
            }
            disableEnable(e.sendBtn, 0);
          }
        }
      }
    },
    error: function (a, c, b) {
      if (c === "timeout") {
        this.tryCount++;
        if (this.tryCount <= this.retryLimit) {
          $.ajax(this);
          return;
        }
      }
      if (e.msgalert === false) {
        g.html(getMessage("danger", false, ajaxError(c)));
      } else {
        Msg.danger(ajaxError(c), MSGBOXTIME);
      }
      if (e.loadingicon === false) {
        f.val(h);
      } else {
        hideshow(e.loadingicon, 0);
      }
      disableEnable(e.sendBtn, 0);
    },
  });
}
function mobileAfterActivted(b) {
  clearInterval(mobileCounter);
  var a = b == "1" ? app.url + "change-password/" : app.url + "account-active/";
  setTimeout(function () {
    $(location).attr("href", a);
  }, REDIRECTTIME);
}
function showMobileResendCode() {
  var a = $("#mobie_active_timer");
  a.text("180");
  var b = parseInt(a.text());
  mobileCounter = setInterval(function () {
    b--;
    a.text(b);
    if (b <= 0) {
      clearInterval(mobileCounter);
      $("#active_mobile_container").addClass("hidden");
      $("#resend_mobile_code_container").removeClass("hidden");
    }
  }, 1000);
}
function showMobileActiveForm() {
  var a = $("#mobie_active_timer"),
    b = $("#active_mobile_container"),
    c = $("#resend_mobile_code_container");
  b.removeClass("hidden");
  c.addClass("hidden");
  a.text("180");
  var d = parseInt(a.text());
  mobileCounter = setInterval(function () {
    d--;
    a.text(d);
    if (d <= 0) {
      clearInterval(mobileCounter);
      b.addClass("hidden");
      c.removeClass("hidden");
    }
  }, 1000);
}
function callbackSuccess(d, e) {
  switch (e) {
    case "loginModal":
      $("#loginModal").modal("hide");
      var f = $(location).attr("href");
      setTimeout(function () {
        $(location).attr("href", f);
      }, REDIRECTTIME);
      break;
    case "afterSendInbox":
    case "afterSendSMS":
      $("#messageModal").modal("hide");
      break;
    case "showpopUpPremium":
      if ($("#popUpPremium").length !== 0) {
        $("#popUpPremium").modal("show");
      }
      break;
    case "afterSendReplySMS":
      $("#replysmsModal").modal("hide");
      break;
    case "afterSendReplyMsg":
      $("#replymsgModal").modal("hide");
      break;
    case "removeModal":
      $(".removeModal").modal("hide");
      break;
    case "mobileAfterActivted":
      mobileAfterActivted(d.auto);
      break;
    case "showMobileActiveForm":
      showMobileActiveForm();
      break;
  }
}
function callbackError(b) {
  switch (b) {
    case "NOT_ENOUGH_SMS_CREDIT":
      Msg.info("لا يوجد رصيد كافي", MSGBOXTIME);
      setTimeout(function () {
        $(location).attr("href", app.url + "my-sms-credit/");
      }, REDIRECTTIME);
      $(".modalDoHidden").modal("hide");
      break;
    case "USER_LOGIN":
      Msg.info("يجب تسجيل الدخول الى حسابك أولا", MSGBOXTIME);
      setTimeout(function () {
        $(location).attr("href", app.url + "login/");
      }, REDIRECTTIME);
      break;
  }
}
function json_Find(h, e, g) {
  var f = [];
  $.each(h, function () {
    var a = this;
    $.each(a, function (b, c) {
      if (g == c && b == e) {
        f.push(a);
      }
    });
  });
  return f;
}
function ltrim(c, d) {
  d = d || "\\s";
  return c.replace(new RegExp("^[" + d + "]+", "g"), "");
}
function internationalCode(g, h) {
  var g = ltrim(g, ""),
    j = $("#" + h).text(),
    k = j.length,
    f = ltrim(g, "0,+");
  if (!k || !f) {
    return false;
  }
  return !(f.substring(0, k) == j);
}
function ar_en_letters_only(b) {
  var a = /^[\u0621-\u064A\040a-zA-Z]+$/;
  return a.test(b);
}
function hideshow(c, d) {
  if (d) {
    $("#" + c).css("visibility", "visible");
  } else {
    $("#" + c).css("visibility", "hidden");
  }
}
function disableEnable(c, d) {
  if (d) {
    $("#" + c).attr("disabled", "disabled");
  } else {
    $("#" + c).removeAttr("disabled");
  }
}
function getMessage(d, e, f) {
  var e = e !== false ? "<strong>" + e + "</strong> " : "";
  return (
    '<div class="alert alert-' +
    d +
    ' alert-dismissible fade in" role="alert">\n<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n' +
    e +
    f +
    " \n</div>"
  );
}
function error(f, d, e) {
  hideshow(f, d);
  if (e) {
    $("#" + f).html(e);
  }
}
function activeMobileBtns(d, f, e) {
  d.text(f);
  disableEnable("mobile_active", e);
  disableEnable("mobile_active_code", e);
}
function activeEmailBtns(d, f, e) {
  d.text(f);
  disableEnable("email_active", e);
  disableEnable("email_active_code", e);
}
function validateEmail(d) {
  var c = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return c.test(d);
}
function modalShow(c, d) {
  $("#myModalLabel").html(c);
  $("#modalcontents").html(d);
  $("#myModal").modal("show");
}
function isArabic(c) {
  var d = /[\u0600-\u06FF]/;
  return d.test(c);
}
function smsInfo() {
  var o = $("#sms"),
    l = $.trim(o.val()),
    h = parseInt(l.length),
    j = $("#smsMessageCount"),
    k = $("#sc");
  $("#lettersCount").text(h);
  if (!h) {
    j.text("0");
    k.val("0");
    return false;
  }
  if (isArabic(l)) {
    var m = h > 70 ? 67 : 70;
  } else {
    var m = h > 160 ? 153 : 160;
  }
  var n = Math.ceil(h / m);
  j.text(n);
  k.val(n);
}
function addComment(e) {
  var d = $("#commentBtn"),
    c = d.val();
  d.val("جاري الارسال ...");
  disableEnable("commentBtn", 1);
  $.ajax({
    type: "POST",
    url: app.root + "ajax/sendcomment.php?lang=" + app.lang,
    cache: false,
    data: $("#form-comment").serialize(),
    dataType: "json",
    timeout: TIMEOUTVALUE,
    tryCount: 0,
    retryLimit: 3,
    success: function (a) {
      if (parseInt(a.status) === 1) {
        if (e) {
          Msg.success(a.txt, MSGBOXTIME);
          $(".removeModal").modal("hide");
        } else {
          $("#commentcontainer").html(getMessage("success", false, a.txt));
        }
      } else {
        if (parseInt(a.status) === 0) {
          Msg.danger(a.txt, MSGBOXTIME);
          disableEnable("commentBtn", 0);
          d.val(c);
        }
      }
    },
    error: function (a, g, b) {
      if (g === "timeout") {
        this.tryCount++;
        if (this.tryCount <= this.retryLimit) {
          $.ajax(this);
          return;
        }
      }
      Msg.danger(ajaxError(g), MSGBOXTIME);
      d.val(c);
      disableEnable("commentBtn", 0);
    },
  });
}
function comment(g) {
  var d = $("#comments"),
    e = parseInt($("#adid").val()),
    f = d.find("nav");
  if (!$(".loadingcomment").length) {
    d.prepend(loadingcomment);
    if (f.length) {
      f.hide();
    }
  }
  $("html, body").animate(
    { scrollTop: $(".loadingcomment").offset().top - 150 },
    300
  );
  $.ajax({
    type: "GET",
    url: app.root + "ajax/comments.php?lang=" + app.lang,
    data: { page: g, adid: e },
    dataType: "html",
    timeout: TIMEOUTVALUE,
    tryCount: 0,
    retryLimit: 3,
    cache: false,
    success: function (a) {
      d.html(a);
    },
    error: function (a, c, b) {
      if (c === "timeout") {
        this.tryCount++;
        if (this.tryCount <= this.retryLimit) {
          $.ajax(this);
          return;
        }
      }
      $(".loadingbg").remove();
      if (f.length) {
        f.show();
      }
      Msg.danger(ajaxError(c), MSGBOXTIME);
    },
  });
}
function scrollToElement(b) {
  $(window).scrollTop(b.offset().top).scrollLeft(b.offset().left);
}
function windowPopup(f, k, g) {
  var h = screen.width / 2 - k / 2,
    j = screen.height / 2 - g / 2;
  window.open(
    f,
    "",
    "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=" +
      k +
      ",height=" +
      g +
      ",top=" +
      j +
      ",left=" +
      h
  );
}
function confirm(k, m, h, g, j) {
  var l = $(
    '<div  id="confirmModal"  class="modal fade confirmModal font1" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true" ><div class="modal-dialog"> <div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title" id="confirmModalLabel">' +
      k +
      '</h4></div><div class="modal-body"><p>' +
      m +
      '</p></div><div class="modal-footer"><div class="btn-group"><a href="#" rel="nofollow"  id="okButton" class="btn btn-success btn-sm">' +
      g +
      '</a><a href="#"  rel="nofollow" class="btn btn-danger btn-sm" data-dismiss="modal">' +
      h +
      "</a></div></div></div></div></div>"
  );
  l.find("#okButton").click(function (a) {
    a.preventDefault();
    j();
    l.modal("hide");
  });
  l.modal("show");
}
$(document).ready(function () {
  $('[data-toggle="popover"]').popover();
  function w() {
    $("#loginModal").modal("show");
  }
  if ($("#mobie_active_timer").length) {
    showMobileResendCode();
  }
  $(".clickedBanner").on("click", function (c) {
    c.preventDefault();
    var b = $(this),
      a = b.attr("href"),
      d = parseInt(b.attr("data-id"));
    $.ajax({
      type: "POST",
      url: app.root + "ajax/clicked_banner.php?lang=" + app.lang,
      cache: false,
      data: { banner_id: d },
      dataType: "json",
      timeout: TIMEOUTVALUE,
      tryCount: 0,
      retryLimit: 3,
      success: function (e) {
        $(location).attr("href", a);
      },
      error: function (e, g, f) {
        if (g === "timeout") {
          this.tryCount++;
          if (this.tryCount <= this.retryLimit) {
            $.ajax(this);
            return;
          }
        }
        $(location).attr("href", a);
      },
    });
  });
  $("#account_type").on("change", function () {
    var c = $(this),
      a = $("#office_data"),
      b = c.val();
    if (b === "office") {
      a.removeClass("hidden");
    } else {
      a.addClass("hidden");
    }
  });
  $("#advancedSerachShow").on("click", function (a) {
    a.preventDefault();
    var b = $("#advancedcontainer");
    $("#advancedSerach").show();
    b.remove();
  });
  $("#more_subs_link").on("click", function (a) {
    a.preventDefault();
    $("#more_subs").removeClass("hidden");
    $(this).remove();
  });
  $(".pakageinfo_link").on("click", function (c) {
    c.preventDefault();
    var b = $(this),
      a = b.closest("form").find(".package_details");
    a.toggleClass("hidden");
  });
  $(".package_days_input").on("change", function (f) {
    f.preventDefault();
    var g = $(this),
      b = g.attr("data-pid"),
      d = $("#discount_package_price_" + b),
      j = $("#package_price_" + b),
      c = parseFloat(g.attr("data-price")),
      k = parseInt(g.attr("data-discount")),
      h = " " + app.currency,
      a = parseFloat(g.attr("data-afterdiscount"));
    if (k !== 0) {
      d.text(a + h);
      j.addClass("through-line").text(c + h);
    } else {
      d.empty();
      j.removeClass("through-line").text(c + h);
    }
  });
  $(".selectedModal").on("click", function (d) {
    d.preventDefault();
    var c = $(this),
      j = c.attr("title"),
      h = c.attr("data-contentid"),
      a = c.attr("data-type"),
      g = $("#" + h);
    if (a == "mainCats") {
      var e = $("#city_search_bar option:selected").val(),
        b = g.find(".main_cat_search"),
        f;
      b.each(function () {
        var l = $(this),
          k = l.attr("href");
        if (k.search("city") !== -1) {
          l.attr("href", k.split("?")[0]);
        }
        if (typeof e !== "undefined" && e !== "") {
          k = l.attr("href");
          f = k + "?city=" + e;
          l.attr("href", f);
        }
      });
    }
    modalShow(j, g.html());
  });
  $(function () {
    $(document).on("scroll", function () {
      if ($(window).scrollTop() > 300) {
        $(".scroll-top-wrapper").addClass("show");
      } else {
        $(".scroll-top-wrapper").removeClass("show");
      }
    });
    $(".scroll-top-wrapper").on("click", function () {
      var d = typeof d != "undefined" ? d : 0;
      var c = $("body"),
        a = c.offset(),
        b = a.top;
      $("html, body").animate({ scrollTop: b }, 500, "linear");
    });
  });
  $("#delVisitorAction").on("click", function (a) {
    a.preventDefault();
    var c = $("#delVisitorAction"),
      b = $("#vaction"),
      e = parseInt(b.attr("data-id")),
      d = b.attr("data-key");
    c.val("جاري الارسال ...");
    disableEnable("delVisitorAction", 1);
    disableEnable("soldVisitorAction", 1);
    $.ajax({
      type: "POST",
      url: app.root + "ajax/visitor_del_ad.php?lang=" + app.lang,
      cache: false,
      data: { adid: e, key: d },
      dataType: "json",
      timeout: TIMEOUTVALUE,
      tryCount: 0,
      retryLimit: 3,
      success: function (f) {
        if (parseInt(f.status)) {
          Msg.success(f.txt, MSGBOXTIME);
          setTimeout(function () {
            $(location).attr("href", b.attr("data-url"));
          }, REDIRECTTIME);
          c.val("جاري الآن تحديث الصفحة");
        } else {
          Msg.danger(f.txt, MSGBOXTIME);
          disableEnable("delVisitorAction", 0);
          disableEnable("soldVisitorAction", 0);
          c.val("حذف الإعلان");
        }
      },
      error: function (f, h, g) {
        if (h === "timeout") {
          this.tryCount++;
          if (this.tryCount <= this.retryLimit) {
            $.ajax(this);
            return;
          }
        }
        Msg.danger(ajaxError(h), MSGBOXTIME);
        disableEnable("delVisitorAction", 0);
        disableEnable("soldVisitorAction", 0);
        c.val("حذف الإعلان");
      },
    });
  });
  $("#soldVisitorAction").on("click", function (a) {
    a.preventDefault();
    var c = $("#soldVisitorAction"),
      b = $("#vaction"),
      e = parseInt(b.attr("data-id")),
      d = b.attr("data-key");
    c.val("جاري الارسال ...");
    disableEnable("delVisitorAction", 1);
    disableEnable("soldVisitorAction", 1);
    $.ajax({
      type: "POST",
      url: app.root + "ajax/visitor_sold_ad.php?lang=" + app.lang,
      cache: false,
      data: { adid: e, key: d },
      dataType: "json",
      timeout: TIMEOUTVALUE,
      tryCount: 0,
      retryLimit: 3,
      success: function (f) {
        if (parseInt(f.status)) {
          Msg.success(f.txt, MSGBOXTIME);
          setTimeout(function () {
            $(location).attr("href", b.attr("data-url"));
          }, REDIRECTTIME);
          c.val("جاري الآن تحديث الصفحة");
        } else {
          Msg.danger(f.txt, MSGBOXTIME);
          disableEnable("delVisitorAction", 0);
          disableEnable("soldVisitorAction", 0);
          c.val("تعديل الإعلان الى تم التعاقد");
        }
      },
      error: function (f, h, g) {
        if (h === "timeout") {
          this.tryCount++;
          if (this.tryCount <= this.retryLimit) {
            $.ajax(this);
            return;
          }
        }
        Msg.danger(ajaxError(h), MSGBOXTIME);
        disableEnable("delVisitorAction", 0);
        disableEnable("soldVisitorAction", 0);
        c.val("تعديل الإعلان الى تم التعاقد");
      },
    });
  });
  $(".js-social-share").on("click", function (a) {
    a.preventDefault();
    windowPopup($(this).attr("href"), 500, 400);
  });
  if ($("#showmap").length && app.map == "1") {
    var s = $("#showmap"),
      t = s.attr("data-lat"),
      q = s.attr("data-lng"),
      m = parseInt(s.attr("data-zoom")),
      z;
    var y = {
      zoom: m,
      center: new google.maps.LatLng(t, q),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    };
    var z = new google.maps.Map(document.getElementById("showmap"), y);
    new google.maps.Marker({
      position: z.getCenter(),
      map: z,
      animation: google.maps.Animation.DROP,
    });
  }
  function v() {
    if (app.map != "1") {
      return false;
    }
    var F,
      g,
      j = [],
      g,
      e = $("#addmap").attr("data-type"),
      l = true,
      E = app.lat,
      D = app.lng,
      c = 15;
    if (e == "e") {
      var f = $("#lat").val(),
        h = $("#lng").val(),
        a = parseInt($("#zoom").val());
      if (f != "" && h != "") {
        E = f;
        D = h;
        c = a;
        l = false;
      }
    }
    F = new google.maps.Geocoder();
    var b = { zoom: c, mapTypeId: google.maps.MapTypeId.ROADMAP };
    g = new google.maps.Map(document.getElementById("addmap"), b);
    if (!!navigator.geolocation && l) {
      navigator.geolocation.getCurrentPosition(
        function (G) {
          var C = new google.maps.LatLng(G.coords.latitude, G.coords.longitude);
          g.setCenter(C);
        },
        function (C) {
          var G = new google.maps.LatLng(E, D);
          g.setCenter(G);
        }
      );
    } else {
      var k = new google.maps.LatLng(E, D);
      g.setCenter(k);
      if (!l) {
        n(k, g);
      }
    }
    $("#searchmap").click(function () {
      var C = $("#searchtxt").val();
      if (C == "") {
        alert("اكتب أسم المدينة أو المنطقة المراد البحث عنها");
        return false;
      }
      F.geocode({ address: C }, function (G, H) {
        if (H == google.maps.GeocoderStatus.OK) {
          g.setZoom(15);
          g.setCenter(G[0].geometry.location);
        } else {
          alert("غير قادر على تحدد المكان رجاء البحث عن مكان أكثر شهرة");
        }
      });
    });
    function d() {
      if (j) {
        for (i in j) {
          j[i].setMap(null);
        }
        j.length = 0;
      }
    }
    function n(H, C) {
      d();
      var G = new google.maps.Marker({ position: H, map: C, draggable: true });
      C.panTo(H);
      j.push(G);
      google.maps.event.addListener(G, "dragend", function (I) {
        $("#lat").val(I.latLng.lat());
        $("#lng").val(I.latLng.lng());
        $("#zoom").val(C.getZoom());
      });
      google.maps.event.addListener(C, "zoom_changed", function () {
        $("#zoom").val(C.getZoom());
      });
    }
    $("#addmarker").click(function (G) {
      G.preventDefault();
      var C = g.getCenter();
      n(C, g);
      $("#lat").val(C.lat());
      $("#lng").val(C.lng());
      $("#zoom").val(g.getZoom());
    });
  }
  if ($("#addmap").length && app.map == "1") {
    v();
  }
  if (location.hash === "#comment") {
    scrollToElement($("#tabcomment"));
  }
  $("#sms").keyup(function () {
    smsInfo();
  });
  $(".collapseicondata").click(function (a) {
    a.preventDefault();
    $(this)
      .find(".fa-plus-square, .fa-minus-square")
      .toggleClass("fa-plus-square fa-minus-square");
  });
  $(".collapseIcon")
    .on("shown.bs.collapse", function () {
      var b = $(this),
        e = $("#" + b.attr("data-icon")),
        c = b.attr("data-modal");
      switch (c) {
        case "smsInbox":
          if (b.attr("data-viewed") !== "1") {
            e.closest("tr").find(".smstxt").removeClass("bold");
            var d = parseInt(b.attr("data-id"));
            u("sms", d);
            b.attr("data-viewed", "1");
          }
          break;
        case "msgInbox":
          if (b.attr("data-viewed") !== "1") {
            e.closest("tr").find(".msgtxt").removeClass("bold");
            var d = parseInt(b.attr("data-id"));
            u("msg", d);
            b.attr("data-viewed", "1");
          }
          break;
        case "comment":
          if (b.attr("data-viewed") !== "1") {
            e.closest("tr").find(".msgtxt").removeClass("bold");
            var d = parseInt(b.attr("data-id"));
            u("comment", d);
            b.attr("data-viewed", "1");
          }
          break;
        case "favAdsComments":
          if (b.attr("data-viewed") !== "1") {
            e.closest("tr").find(".msgtxt").removeClass("bold");
            var a = parseInt(b.attr("data-favid"));
            u("favAdsComments", a);
            b.attr("data-viewed", "1");
          }
          break;
      }
      e.removeClass("fa fa-chevron-down").addClass("fa fa-chevron-up");
    })
    .on("hidden.bs.collapse", function () {
      $("#" + $(this).attr("data-icon"))
        .removeClass("fa fa-chevron-up")
        .addClass("fa fa-chevron-down");
    });
  function u(b, a) {
    $.ajax({
      type: "GET",
      url: app.root + "ajax/update_viewed_message.php?lang=" + app.lang,
      cache: false,
      data: { type: b, id: a, _token: app._token },
      dataType: "json",
      timeout: TIMEOUTVALUE,
      tryCount: 0,
      retryLimit: 3,
    });
  }
  $("#show-mobile-number").click(function () {
    $("#mobile-number").removeClass("hidethis");
    $(this).remove();
  });
  if ($("#popAdconditions").length !== 0) {
    if ($.cookie(app.ckprefix + "phac") !== "1") {
      $("#popAdconditions").modal({ backdrop: "static", keyboard: false });
    }
  }
  if ($("#popUpHomeBanner").length !== 0) {
    $("#popUpHomeBanner").modal("show");
  }
  if ($("#btnAgreeAdconditions").length !== 0) {
    $("#btnAgreeAdconditions").click(function (a) {
      a.preventDefault();
      $("#popAdconditions").modal("hide");
      $.cookie(app.ckprefix + "phac", "1", { path: "/", expires: 30 });
    });
  }
  $('[data-toggle="tooltip"]').tooltip();
  $("input, textarea").placeholder({ customClass: "my-placeholder" });
  function x(a, g, n, K, e, b, h, P) {
    var M, l, j;
    switch (a) {
      case "sub1":
        M = jsSubs.sub1;
        j = "sub1";
        l = "getsub2";
        $("#sub2_ct").html(
          '<input type="hidden"  class="form-control" name="sub2" id="sub2" value="">'
        );
        $("#sub3_ct").html(
          '<input type="hidden"  class="form-control" name="sub3" id="sub3" value="">'
        );
        $("#sub4_ct").html(
          '<input type="hidden"  class="form-control" name="sub4" id="sub4" value="">'
        );
        break;
      case "sub2":
        M = jsSubs.sub2;
        j = "sub2";
        l = "getsub3";
        $("#sub3_ct").html(
          '<input type="hidden"  class="form-control" name="sub3" id="sub3" value="">'
        );
        $("#sub4_ct").html(
          '<input type="hidden"  class="form-control" name="sub4" id="sub4" value="">'
        );
        break;
      case "sub3":
        M = jsSubs.sub3;
        j = "sub3";
        l = "getsub4";
        $("#sub4_ct").html(
          '<input type="hidden"  class="form-control" name="sub4" id="sub4" value="">'
        );
        break;
      case "sub4":
        M = jsSubs.sub4;
        j = "sub4";
        l = "";
        break;
      default:
        $("#subcat_ct").html(
          '<input type="hidden"  class="form-control" name="sub1" id="sub1" value="">'
        );
        $("#sub2_ct").html(
          '<input type="hidden"  class="form-control" name="sub2" id="sub2" value="">'
        );
        $("#sub3_ct").html(
          '<input type="hidden"  class="form-control" name="sub3" id="sub3" value="">'
        );
        $("#sub4_ct").html(
          '<input type="hidden"  class="form-control" name="sub4" id="sub4" value="">'
        );
        return;
    }
    if (M) {
      var d = JSON.parse(M),
        O = "",
        N = '<div class="form-group">',
        I = "</div>";
      if (g == "1") {
        N = "";
        I = "";
      }
      var J = json_Find(d, "catid", n),
        c = '<option value="">' + K + "</option>" + O;
      if (J && J.length) {
        $.each(J, function (D, C) {
          c +=
            "<option value='" +
            C.subid +
            "' data-subname='" +
            C.subname +
            "' data-slug='" +
            C.slug +
            "' >" +
            C.name +
            "</option>";
        });
        var f =
          '<div class="col-md-' +
          e +
          '">' +
          N +
          '<select name="' +
          j +
          '" id="' +
          j +
          '"  class="form-control ' +
          l +
          " " +
          b +
          '" >' +
          c +
          "</select>" +
          I +
          "</div>";
        h.html(f);
      } else {
        var k =
          '<input type="hidden"  class="form-control" name="' +
          j +
          '" id="' +
          j +
          '" value="">';
        h.html(k);
      }
      if (g != "1") {
        for (var L = 1; L <= 4; L++) {
          P.formValidation("addField", "sub" + L, {
            validators: { notEmpty: { message: "مطلوب" } },
          });
        }
      }
    }
  }
  function p(a) {
    a.find("option").each(function () {
      $(this).show();
    });
  }
  function B(c, b) {
    var a = c.closest(".adtype_ct");
    c.val("").change();
    if (b != "") {
      a.removeClass("hide");
      c.find("option").each(function () {
        var e = $(this),
          f = e.attr("data-catids");
        e.show();
        if (f !== undefined) {
          var d = f.split(",");
          if ($.inArray(b, d) === -1) {
            e.hide();
          }
        }
      });
    } else {
      a.addClass("hide");
      p(c);
    }
  }
  $(".subfields").on("change", function () {
    var j = $(this),
      k = j.val(),
      l = j.find("option:selected"),
      b = l.attr("data-subname"),
      g = b ? b : "النوع",
      f = $("#subcat_ct"),
      e = j.closest("form"),
      a = e.attr("data-search"),
      d,
      c,
      h = e.find("#adtype");
    B(h, k);
    if (a == "1") {
      d = "";
      c = "12";
    } else {
      d = " input-lg";
      c = "6";
    }
    $(".subs").empty();
    x("sub1", a, k, g, c, d, f, e);
  });
  function r() {
    var d = $("#headerSearchForm"),
      b = d.attr("data-cat"),
      a = d.attr("data-city"),
      c = app.base;
    if (a && a !== undefined) {
      c += "city/" + a + "/";
    }
    c += b + "/";
    var h = $("#headerSearchForm #sub1")
        .find("option:selected")
        .attr("data-slug"),
      g = $("#headerSearchForm #sub2")
        .find("option:selected")
        .attr("data-slug"),
      f = $("#headerSearchForm #sub3")
        .find("option:selected")
        .attr("data-slug"),
      e = $("#headerSearchForm #sub4")
        .find("option:selected")
        .attr("data-slug");
    if (h && h !== undefined) {
      c += h + "/";
    }
    if (g && g !== undefined) {
      c += g + "/";
    }
    if (f && f !== undefined) {
      c += f + "/";
    }
    if (e && e !== undefined) {
      c += e + "/";
    }
    d.attr("action", c);
  }
  $(".domupdate").on("change", ".getsub2", function () {
    var h = $(this),
      j = h.val(),
      k = h.find("option:selected"),
      a = k.attr("data-subname"),
      g = a ? a : "الفرع",
      f = $("#sub2_ct"),
      c = h.closest("form"),
      d = c.attr("data-search"),
      b = " input-lg",
      e;
    if (d == "1") {
      e = "4 mrtb3";
    } else {
      e = "6";
    }
    f.empty();
    $("#sub3_ct").empty();
    $("#sub4_ct").empty();
    x("sub2", d, j, g, e, b, f, c);
    if (d == "1") {
      r();
    }
  });
  $(".domupdate").on("change", ".getsub3", function () {
    var h = $(this),
      j = h.val(),
      k = h.find("option:selected"),
      a = k.attr("data-subname"),
      g = a ? a : "الفرع",
      f = $("#sub3_ct"),
      c = h.closest("form"),
      d = c.attr("data-search"),
      b = " input-lg",
      e;
    if (d == "1") {
      e = "4 mrtb3";
    } else {
      e = "6";
    }
    f.empty();
    $("#sub4_ct").empty();
    x("sub3", d, j, g, e, b, f, c);
    if (d == "1") {
      r();
    }
  });
  $(".domupdate").on("change", ".getsub4", function () {
    var j = $(this),
      k = j.val(),
      a = j.find("option:selected"),
      b = a.attr("data-subname"),
      h = b ? b : "الفرع",
      g = $("#sub4_ct"),
      d = j.closest("form"),
      e = d.attr("data-search"),
      c = " input-lg",
      f;
    if (e == "1") {
      f = "4 mrtb3";
    } else {
      f = "6";
    }
    g.empty();
    x("sub4", e, k, h, f, c, g, d);
    if (e == "1") {
      r();
    }
  });
  $(".domupdate").on("change", "#sub4", function () {
    var a = $("#headerSearchForm").attr("data-search");
    if (a) {
      r();
    }
  });
  $("#activeEmailBtn").on("click", function () {
    var a = $("#activeEmailBtn");
    var c = $("#email_active").val();
    var b = $("#email_active_code").val();
    activeEmailBtns(a, "جاري الإرسال ...", 1);
    if (!$.trim(c).length || !$.trim(b).length || !validateEmail(c)) {
      if (!$.trim(c).length) {
        Msg.danger("البريد الإلكتروني مطلوب", MSGBOXTIME);
      } else {
        if (!validateEmail(c)) {
          Msg.danger("البريد الإلكتروني غير صحيح", MSGBOXTIME);
        } else {
          Msg.danger("كود تفعيل البريد مطلوب", MSGBOXTIME);
        }
      }
      activeEmailBtns(a, "تفعيل البريد", 0);
    } else {
      $.ajax({
        type: "GET",
        url: app.root + "ajax/active_email.php?lang=" + app.lang,
        cache: false,
        data: { email: c, email_active_code: b },
        dataType: "json",
        timeout: TIMEOUTVALUE,
        tryCount: 0,
        retryLimit: 3,
        success: function (d) {
          switch (parseInt(d.status)) {
            case 1:
              Msg.success(d.txt, MSGBOXTIME);
              setTimeout(function () {
                $(location).attr("href", app.url + "account-active/");
              }, REDIRECTTIME);
              break;
            case 0:
              Msg.danger(d.txt, MSGBOXTIME);
              break;
            default:
              Msg.warning(d.txt, MSGBOXTIME);
              break;
          }
          if (parseInt(d.status) === 1) {
            activeEmailBtns(a, "تم التفعيل", 1);
          } else {
            activeEmailBtns(a, "تفعيل البريد", 0);
          }
        },
        error: function (d, f, e) {
          if (f === "timeout") {
            this.tryCount++;
            if (this.tryCount <= this.retryLimit) {
              $.ajax(this);
              return;
            }
          }
          Msg.danger(ajaxError(f), MSGBOXTIME);
          activeEmailBtns(a, "تفعيل البريد", 0);
        },
      });
    }
  });
  $("#resendEmailCode").on("click", function () {
    var a = $("#resendEmailCode");
    var b = $("#email_active").val();
    activeEmailBtns(a, "جاري الإرسال ...", 1);
    if (!$.trim(b).length || !validateEmail(b)) {
      if (!$.trim(b).length) {
        Msg.danger("البريد الإلكتروني مطلوب", MSGBOXTIME);
      } else {
        Msg.danger("البريد الإلكتروني غير صحيح", MSGBOXTIME);
      }
      activeEmailBtns(a, "أعادة ارسال الكود", 0);
    } else {
      $.ajax({
        type: "GET",
        url: app.root + "ajax/resend_email_code.php?lang=" + app.lang,
        cache: false,
        data: { email: b },
        dataType: "json",
        timeout: TIMEOUTVALUE,
        tryCount: 0,
        retryLimit: 3,
        success: function (c) {
          switch (parseInt(c.status)) {
            case 1:
              Msg.success(c.txt, MSGBOXTIME);
              break;
            case 0:
              Msg.danger(c.txt, MSGBOXTIME);
              break;
            default:
              Msg.warning(c.txt, MSGBOXTIME);
              break;
          }
          activeEmailBtns(a, "أعادة ارسال الكود", 0);
        },
        error: function (c, e, d) {
          if (e === "timeout") {
            this.tryCount++;
            if (this.tryCount <= this.retryLimit) {
              $.ajax(this);
              return;
            }
          }
          Msg.danger(ajaxError(e), MSGBOXTIME);
          activeEmailBtns(a, "أعادة ارسال الكود", 0);
        },
      });
    }
  });
  $(".deleteimage").click(function (a) {
    a.preventDefault();
    var b = $(this),
      e = b.closest("li"),
      d = e.find(".loadingimage"),
      f = e.attr("id"),
      c = b.attr("data-type");
    b.addClass("hidden");
    d.removeClass("hidden");
    $.ajax({
      type: "GET",
      url: app.root + "ajax/delimage.php?lang=" + app.lang,
      data: { imgid: f, type: c },
      dataType: "json",
      timeout: TIMEOUTVALUE,
      tryCount: 0,
      retryLimit: 3,
      cache: false,
      success: function (g) {
        if (parseInt(g.status) === 1) {
          e.remove();
        } else {
          b.removeClass("hidden");
          d.addClass("hidden");
          Msg.danger(g.txt, MSGBOXTIME);
        }
      },
      error: function (g, j, h) {
        if (j === "timeout") {
          this.tryCount++;
          if (this.tryCount <= this.retryLimit) {
            $.ajax(this);
            return;
          }
        }
        b.removeClass("hidden");
        d.addClass("hidden");
        Msg.danger(ajaxError(j), MSGBOXTIME);
      },
    });
  });
  function o(c, a) {
    var b = {};
    switch (c) {
      case "adverts":
        switch (a) {
          case "sold":
            b.heading = "تم التعاقد";
            b.question = "هل تم التعاقد لهذا الإعلان ؟";
            break;
          case "unsold":
            b.heading = "إلغاء التعاقد";
            b.question = "هل تريد إلغاء التعاقد لهذا الإعلان ؟";
            break;
          case "renew":
            b.heading = "تجديد الإعلان";
            b.question = "تجديد عرض الإعلان لمدة 3 شهور ؟";
            break;
          case "del":
            b.heading = "حذف";
            b.question = "هل تريد حذف الإعلان ؟";
            break;
          case "fixed":
            b.heading = "تثبيت";
            b.question = "هل تريد تثبيت الإعلان ؟";
            break;
          case "unfixed":
            b.heading = "إلغاء التثبيت";
            b.question = "هل تريد إلغاء تثبيت الإعلان ؟";
            break;
          case "undel":
            b.heading = "إلغاء الحذف";
            b.question = "هل تريد إلغاء حذف الإعلان ؟";
            break;
        }
        break;
      case "favorite":
        switch (a) {
          case "del":
            b.heading = "حذف";
            b.question = "هل تريد حذف الإعلان من قائمة المفضلة ؟";
            break;
        }
        break;
      case "sms":
      case "msg":
        switch (a) {
          case "delinbox":
          case "deloutbox":
            b.heading = "حذف";
            b.question = "هل تريد حذف الرسالة ؟";
            break;
        }
        break;
      case "unfollow":
        switch (a) {
          case "unfollow_member":
            b.heading = "إلغاء المتابعه";
            b.question = "هل تريد إلغاء المتابعه للعضو ؟";
            break;
          case "unfollow_cat":
            b.heading = "إلغاء المتابعه";
            b.question = "هل تريد إلغاء المتابعه لهذا القسم ؟";
            break;
        }
        break;
    }
    return b;
  }
  $(".userDataAction").click(function (j) {
    j.preventDefault();
    var a = $(this),
      f = parseInt(a.attr("data-id")),
      d = a.attr("data-type"),
      c = a.attr("data-action"),
      e = a.attr("data-mode"),
      h = $("#row" + f),
      b = o(d, c);
    var g = function () {
      $.ajax({
        type: "GET",
        url: app.root + "ajax/deldata.php?lang=" + app.lang,
        cache: false,
        data: { id: f, type: d, action: c, _token: app._token },
        dataType: "json",
        timeout: TIMEOUTVALUE,
        tryCount: 0,
        retryLimit: 3,
        success: function (k) {
          if (parseInt(k.status) === 1) {
            var C;
            if (e == "alert") {
              switch (c) {
                case "fixed":
                  C = "تم تثبيت الإعلان بنجاح";
                  a.attr("data-action", "unfixed")
                    .html("إلغاء التثبيت")
                    .removeClass("btn-warning")
                    .addClass("btn-bitbucket");
                  break;
                case "unfixed":
                  C = "تم إلغاء تثبيت الإعلان بنجاح";
                  a.attr("data-action", "fixed")
                    .html("تثبيت؟")
                    .removeClass("btn-bitbucket")
                    .addClass("btn-warning");
                  break;
                case "sold":
                  C = "تم التعاقد على الإعلان بنجاح";
                  a.attr("data-action", "unsold")
                    .html("إلغاء التعاقد")
                    .removeClass("btn-info")
                    .addClass("btn-instagram");
                  var n = a.closest("div").find(".btn-fixed");
                  n.attr("data-action", "fixed")
                    .html("تثبيت؟")
                    .removeClass("btn-bitbucket")
                    .addClass("btn-warning");
                  break;
                case "unsold":
                  C = "تم إلغاء التعاقد على الإعلان بنجاح";
                  a.attr("data-action", "sold")
                    .html("تم التعاقد؟")
                    .removeClass("btn-instagram")
                    .addClass("btn-info");
                  break;
              }
              Msg.success(C, MSGBOXTIME);
            } else {
              h.remove();
              if ($(".doRefresh").length) {
                if (!$(".actionAfterDel").length) {
                  var l = $(location).attr("href").split("&")[0];
                  $(location).attr("href", l);
                }
              }
            }
          } else {
            Msg.danger(k.txt, MSGBOXTIME);
          }
        },
        error: function (n, l, k) {
          if (l === "timeout") {
            this.tryCount++;
            if (this.tryCount <= this.retryLimit) {
              $.ajax(this);
              return;
            }
          }
          Msg.danger(ajaxError(l), MSGBOXTIME);
        },
      });
    };
    confirm(b.heading, b.question, "لا", "نعم", g);
  });
  $(".payment").click(function (a) {
    a.preventDefault();
    var b = $(this),
      f = b.closest("div"),
      d = b.attr("data-type"),
      e = parseInt(b.attr("data-pid")),
      c = b.val();
    b.val("جاري الارسال ...");
    b.attr("disabled", "disabled");
    $.ajax({
      type: "POST",
      url: app.root + "ajax/payment.php",
      data: { pid: e, type: d, _token: app._token },
      dataType: "html",
      timeout: TIMEOUTVALUE,
      tryCount: 0,
      retryLimit: 3,
      cache: false,
      success: function (h) {
        if (h == "0" || h == "") {
          var g =
            '<div><a href="' +
            app.base +
            "banks-accounts/?package=" +
            e +
            '" title="تحويل بنكى" class="btn btn-success btn-block">أدفع بواسطة حوالة بنكية</a></div>';
          f.html(g);
          b.removeAttr("disabled");
        } else {
          f.html(h);
        }
      },
      error: function (g, j, h) {
        if (j === "timeout") {
          this.tryCount++;
          if (this.tryCount <= this.retryLimit) {
            $.ajax(this);
            return;
          }
        }
        Msg.danger(ajaxError(j), MSGBOXTIME);
        b.val(c);
        b.removeAttr("disabled");
      },
    });
  });
  app.Favorite = function (e, g) {
    e.removeClass("disabled");
    var b = e.find(".favcolor"),
      a = "fa-heart",
      f = "fa-heart-o",
      d = "حذف من المفضلة",
      c = "أضف الى المفضلة";
    if (g == "add") {
      b.removeClass(f).addClass(a);
      e.attr("data-method", "remove");
      e.attr("title", d);
    } else {
      b.removeClass(a).addClass(f);
      e.attr("title", c);
      e.attr("data-method", "add");
    }
  };
  $(".followUnfollowBtn").click(function (c) {
    c.preventDefault();
    if (app.uo === "0" || typeof app.uo === "undefined") {
      w();
    } else {
      var d = $(this),
        a = parseInt(d.attr("data-userid")),
        e = d.attr("data-method"),
        b = e == "follow" ? "follow_member.php" : "unfollow_member.php";
      if (d.hasClass("disabled")) {
        return false;
      }
      d.addClass("disabled");
      $.ajax({
        type: "POST",
        url: app.root + "ajax/" + b + "?lang=" + app.lang,
        cache: false,
        data: { userid: a, _token: app._token },
        dataType: "json",
        timeout: TIMEOUTVALUE,
        tryCount: 0,
        retryLimit: 3,
        success: function (f) {
          if (parseInt(f.status) === 1) {
            app.followMembers(d, e);
            Msg.success(f.txt, MSGBOXTIME);
          } else {
            if (parseInt(f.status) === 0) {
              Msg.danger(f.txt, MSGBOXTIME);
            } else {
              Msg.info(f.txt, MSGBOXTIME);
            }
          }
          d.removeClass("disabled");
        },
        error: function (f, h, g) {
          if (h === "timeout") {
            this.tryCount++;
            if (this.tryCount <= this.retryLimit) {
              $.ajax(this);
              return;
            }
          }
          Msg.danger(ajaxError(h), MSGBOXTIME);
          d.removeClass("disabled");
        },
      });
    }
  });
  app.followMembers = function (c, a) {
    var f = c.find("i"),
      d = c.find(".follow_text"),
      g = "fa-check-circle-o",
      b = "fa-plus",
      j = c.attr("data-type") === "office" ? "المكتب" : "العضو",
      e = "تابع " + j,
      h = "إلغاء المتابعه";
    if (a === "follow") {
      f.removeClass(b).addClass(g);
      c.attr("data-method", "unfollow");
      c.attr("title", h);
      d.text(h);
    } else {
      f.removeClass(g).addClass(b);
      c.attr("title", e);
      c.attr("data-method", "follow");
      d.text(e);
    }
  };
  $(".followUnfollowMainCatogry").click(function (c) {
    c.preventDefault();
    if (app.uo === "0" || typeof app.uo === "undefined") {
      w();
    } else {
      var d = $(this),
        a = parseInt(d.attr("data-catid")),
        e = d.attr("data-method"),
        b = e == "follow" ? "follow_cat.php" : "unfollow_cat.php";
      if (d.hasClass("disabled")) {
        return false;
      }
      d.addClass("disabled");
      $.ajax({
        type: "POST",
        url: app.root + "ajax/" + b + "?lang=" + app.lang,
        cache: false,
        data: { catid: a, _token: app._token },
        dataType: "json",
        timeout: TIMEOUTVALUE,
        tryCount: 0,
        retryLimit: 3,
        success: function (f) {
          if (parseInt(f.status) === 1) {
            app.followMainCatogry(d, e);
            Msg.success(f.txt, MSGBOXTIME);
          } else {
            if (parseInt(f.status) === 0) {
              Msg.danger(f.txt, MSGBOXTIME);
            } else {
              Msg.info(f.txt, MSGBOXTIME);
            }
          }
          d.removeClass("disabled");
        },
        error: function (f, h, g) {
          if (h === "timeout") {
            this.tryCount++;
            if (this.tryCount <= this.retryLimit) {
              $.ajax(this);
              return;
            }
          }
          Msg.danger(ajaxError(h), MSGBOXTIME);
          d.removeClass("disabled");
        },
      });
    }
  });
  app.followMainCatogry = function (d, a) {
    var g = d.find("i"),
      e = d.find(".follow_text"),
      c = d.attr("data-catname"),
      h = "fa-check-circle-o",
      b = "fa-plus",
      f = "تابع ",
      j = "إلغاء المُتابعة";
    if (a === "follow") {
      g.removeClass(b).addClass(h);
      d.attr("data-method", "unfollow");
      d.attr("title", j);
      e.text(j);
    } else {
      g.removeClass(h).addClass(b);
      d.attr("title", f);
      d.attr("data-method", "follow");
      e.text(f);
    }
  };
  $(".loginFirst").click(function (a) {
    a.preventDefault();
    if (app.uo === "0" || typeof app.uo === "undefined") {
      w();
    } else {
      Msg.danger(
        "حسابك غير مفعل <a rel='nofollow' href='" +
          app.url +
          "account-active/'>اضغط هنا</a> لتفعيل حسابك",
        MSGBOXTIME
      );
    }
    return;
  });
  $(".shareSocialBtn").click(function (c) {
    c.preventDefault();
    var b = $(this);
    if (navigator.share) {
      var e = b.attr("data-title"),
        a = b.attr("data-link"),
        d = b.attr("data-text");
      navigator.share({ title: e, text: d, url: a });
    } else {
      if (b.attr("data-model") == "1") {
        $("#shareModal").modal("show");
      } else {
        return false;
      }
    }
  });
  $(".addFavoriteDialog").click(function (c) {
    c.preventDefault();
    if (app.uo === "0" || typeof app.uo === "undefined") {
      w();
    } else {
      var d = $(this),
        b = parseInt(d.attr("data-id")),
        e = d.attr("data-method"),
        a = e == "add" ? "favorite_add.php" : "favorite_remove.php";
      if (d.hasClass("disabled")) {
        return false;
      }
      d.addClass("disabled");
      $.ajax({
        type: "POST",
        url: app.root + "ajax/" + a + "?lang=" + app.lang,
        cache: false,
        data: { id: b, _token: app._token },
        dataType: "json",
        timeout: TIMEOUTVALUE,
        tryCount: 0,
        retryLimit: 3,
        success: function (f) {
          if (parseInt(f.status) === 1) {
            app.Favorite(d, e);
            Msg.success(f.txt, MSGBOXTIME);
          } else {
            if (parseInt(f.status) === 0) {
              Msg.danger(f.txt, MSGBOXTIME);
              d.removeClass("disabled");
            } else {
              Msg.info(f.txt, MSGBOXTIME);
              d.removeClass("disabled");
            }
          }
        },
        error: function (f, h, g) {
          if (h === "timeout") {
            this.tryCount++;
            if (this.tryCount <= this.retryLimit) {
              $.ajax(this);
              return;
            }
          }
          Msg.danger(ajaxError(h), MSGBOXTIME);
          d.removeClass("disabled");
        },
      });
    }
  });
  $(document).on("click", ".privateMessage", function (b) {
    b.preventDefault();
    if (app.uo === "0" || typeof app.uo === "undefined") {
      w();
    } else {
      var d = $(this),
        f = parseInt(d.attr("data-userid")),
        c = parseInt(d.attr("data-msgid")),
        g = d.attr("data-username"),
        a = parseInt(d.attr("data-private"));
      var e = $(
        '<div class="modal fade privateMsgModal removeModal" id="privateMsgModal" tabindex="-1" role="dialog" aria-labelledby="privateMsgModalLabel" aria-hidden="true"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> <h4 class="modal-title"><i class="fa fa-envelope-o"></i> رسالة خاصة الى ' +
          g +
          '</h4> </div> <div class="modal-body"> <div class="row"> <div class="col-md-12"> <form role="form" id="formprivateMsg" class="formprivateMsg" autocomplete="off" method="POST"> <div class="row"> <div class="col-md-12"> <div class="form-group rel"> <textarea rows="6" cols="95" name="message" id="message" class="form-control input-lg" placeholder="اكتب رسالتك"></textarea> </div> </div> <div class="col-md-12"> <input type="hidden" name="private" id="private" value="' +
          a +
          '"> <input type="hidden" name="touserid" id="touserid" value="' +
          f +
          '"> <input type="hidden" name="msgid" id="msgid" value="' +
          c +
          '"> <input type="hidden" name="_token" value="' +
          app._token +
          '" /> <input type="submit" class="btn btn-success blocksm" id="privateMsgBtn" name="privateMsgBtn" value="ارسال" /> </div> </div> </form> </div> </div> </div> </div> </div> </div>'
      );
      e.find("#formprivateMsg")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            message: {
              validators: {
                notEmpty: { message: "الرسالة مطلوبة" },
                stringLength: { min: 5, message: "الرسالة قصيرة جدا" },
              },
            },
          },
        })
        .on("success.form.fv", function (h) {
          h.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "privateMsgBtn",
            sendtype: "POST",
            fullurl: "ajax/private_message.php",
            formid: "formprivateMsg",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: false,
            redirecturl: null,
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: "removeModal",
          });
        })
        .on("err.field.fv", function (j, h) {
          if (h.fv.getSubmitButton()) {
            h.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (j, h) {
          if (h.fv.getSubmitButton()) {
            h.fv.disableSubmitButtons(false);
          }
        });
      e.modal("show");
    }
  });
  $(document).on("click", ".sendCommentDialog", function (b) {
    b.preventDefault();
    if (app.uo === "0" || typeof app.uo === "undefined") {
      w();
    } else {
      var c = $(this),
        a = parseInt(c.attr("data-id"));
      var d = $(
        '<div class="modal fade commentModel removeModal" id="commentModel" tabindex="-1" role="dialog" aria-labelledby="commentModelLabel" aria-hidden="true"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> <h4 class="modal-title"><i class="fa fa-commenting"></i> تعليق جديد</h4> </div> <div class="modal-body"> <div class="row"> <div class="col-md-12"> <form role="form" id="form-comment" class="form-comment" autocomplete="off" method="POST"> <div class="row"> <div class="col-md-12"> <div class="form-group rel"> <textarea rows="6" cols="95" name="commentdata" id="commentdata" class="form-control input-lg" placeholder="اكتب تعليقك"></textarea> </div> </div> <div class="col-md-12"> <input type="hidden" name="adid" id="adid" value="' +
          a +
          '"><input type="hidden" name="_token" value="' +
          app._token +
          '" /> <input type="submit" class="btn btn-success blocksm" id="commentBtn" name="commentBtn" value="ارسال" /> </div> </div> </form> </div> </div> </div> </div> </div> </div>'
      );
      d.find("#form-comment")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            commentdata: {
              validators: {
                notEmpty: { message: "التعليق مطلوب" },
                stringLength: { min: 5, message: "التعليق قصير جدا" },
              },
            },
          },
        })
        .on("success.form.fv", function (e) {
          e.preventDefault();
          addComment(true);
        })
        .on("err.field.fv", function (f, e) {
          if (e.fv.getSubmitButton()) {
            e.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (f, e) {
          if (e.fv.getSubmitButton()) {
            e.fv.disableSubmitButtons(false);
          }
        });
      d.modal("show");
    }
  });
  $("#comments").on("click", ".replyComment", function (c) {
    c.preventDefault();
    if (app.uo === "0" || typeof app.uo === "undefined") {
      w();
    } else {
      var d = $(this),
        b = parseInt(d.attr("data-adid")),
        a = parseInt(d.attr("data-comid")),
        f = parseInt(d.attr("data-touserid"));
      var e = $(
        '<div class="modal fade replyCommentModal removeModal" id="replyCommentModal" tabindex="-1" role="dialog" aria-labelledby="replyCommentModalLabel" aria-hidden="true"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> <h4 class="modal-title" ><i class="fa fa-reply"></i> رد على التعليق</h4> </div> <div class="modal-body" ><div class="row"><div class="col-md-12" ><form role="form" id="formreplyComment" class="formreplyComment" autocomplete="off" method="POST"><div class="row"> <div class="col-md-12"> <div class="form-group rel"> <textarea rows="6" cols="95" name="message" id="message" class="form-control input-lg" placeholder="اكتب الرد" ></textarea> </div> </div><div class="col-md-12"> <input type="hidden" name="comadid" id="comadid" value="' +
          b +
          '"> <input type="hidden" name="comid" id="comid" value="' +
          a +
          '"> <input type="hidden" name="touserid" id="touserid" value="' +
          f +
          '"> <input type="hidden" name="_token" value="' +
          app._token +
          '" /> <input type="submit" class="btn btn-success blocksm" id="replyCommentBtn" name="replyCommentBtn" value="ارسال" /> </div> </div></form></div></div></div></div></div></div>'
      );
      e.find("#formreplyComment")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            message: {
              validators: {
                notEmpty: { message: "التعليق مطلوب" },
                stringLength: { min: 5, message: "التعليق قصير جدا" },
              },
            },
          },
        })
        .on("success.form.fv", function (g) {
          g.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "replyCommentBtn",
            sendtype: "POST",
            fullurl: "ajax/replycomment.php",
            formid: "formreplyComment",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: false,
            redirecturl: null,
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: "removeModal",
          });
        })
        .on("err.field.fv", function (h, g) {
          if (g.fv.getSubmitButton()) {
            g.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (h, g) {
          if (g.fv.getSubmitButton()) {
            g.fv.disableSubmitButtons(false);
          }
        });
      e.modal("show");
    }
  });
  $("#abuseAdvert").click(function (a) {
    a.preventDefault();
    if (app.uo === "0" || typeof app.uo === "undefined") {
      w();
    } else {
      var b = $(this),
        d = parseInt(b.attr("data-adid")),
        e = parseInt(b.attr("data-adowner"));
      if (e == app.uo) {
        Msg.info("لا تستطيع التبليغ عن اعلانك", MSGBOXTIME);
        return;
      }
      var c = $(
        '<div  id="advertAbuseModal"  class="modal fade advertAbuseModal removeModal" tabindex="-1" role="dialog" aria-labelledby="advertAbuseModalLabel" aria-hidden="true" ><div class="modal-dialog"> <div class="modal-content"><div class="modal-header mrb2"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title" id="advertAbuseModalLabel">بلغ عن هذا الإعلان</h4></div><form role="form" id="advertAbuseFrm" class="advertAbuseFrm" method="POST" ><div class="modal-body"><div class="row"><div class="col-md-12"><div class="form-group"><select name="bause_type" id="bause_type" class="form-control input-lg"><option value="">اختر نوع المخالفة</option><option value="1" >إعلان مُخل بالآداب</option><option value="2" >قسم الإعلان غير صحيح</option><option value="3" >إحتواء الإعلان على رسائل دعائية</option><option value="4" >إحتيال أو نصب</option><option value="5" >سبب أخر</option></select></div></div><div class="col-md-12"><div class="form-group"> <textarea class="form-control input-lg" name="message" rows="6" placeholder="تفاصيل البلاغ"></textarea></div></div></div></div><div class="modal-footer"><div class="row"><div class="col-md-12"><input type="submit" class="btn  btn-success blocksm" id="advertAbuseBtn" name="advertAbuseBtn" value="ارسال" /><input type="hidden" name="adid" value="' +
          d +
          '" /><input type="hidden" name="_token" value="' +
          app._token +
          '" /></div></div></div></form></div></div></div>'
      );
      c.find("#advertAbuseFrm")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            message: {
              validators: {
                notEmpty: { message: "اكتب المخالفة بالتفاصيل" },
                stringLength: { max: 255, message: "اقصي عدد للحروف 255 حرف" },
              },
            },
            bause_type: { validators: { notEmpty: { message: "اختر السبب" } } },
          },
        })
        .on("success.form.fv", function (f) {
          f.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "advertAbuseBtn",
            sendtype: "POST",
            fullurl: "ajax/advert_abuse.php",
            formid: "advertAbuseFrm",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: false,
            redirecturl: null,
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: "removeModal",
          });
        })
        .on("err.field.fv", function (g, f) {
          if (f.fv.getSubmitButton()) {
            f.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (g, f) {
          if (f.fv.getSubmitButton()) {
            f.fv.disableSubmitButtons(false);
          }
        });
      c.modal("show");
    }
  });
  $("#voting").click(function (a) {
    a.preventDefault();
    if (app.uo === "0" || typeof app.uo === "undefined") {
      w();
    } else {
      var b = $(this),
        d = parseInt(b.attr("data-userid")),
        e = b.attr("data-username");
      if (d == app.uo) {
        Msg.info("لا تستطيع تقييم نفسك", MSGBOXTIME);
        return;
      }
      var c = $(
        '<div id="votingModal" class="modal fade votingModal removeModal" tabindex="-1" role="dialog" aria-labelledby="votingModalLabel" aria-hidden="true"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header mrb2"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> <h4 class="modal-title" id="votingModalLabel">تقييم</h4></div> <form role="form" id="votingFrm" class="votingFrm" method="POST"> <div class="modal-body"> <div class="row"> <div class="col-md-12"> <div class="form-group"> <select name="advise" id="advise" class="form-control input-lg"> <option value="">هل تنصح الأعضاء الأخرين بالتعامل مع  ' +
          e +
          ' ؟</option> <option value="0">لا</option> <option value="1">نعم</option> </select> </div> </div> <div class="col-md-12"> <div class="form-group"> <textarea class="form-control input-lg" name="message" rows="6" placeholder="نرجو ذكر تجربتك"></textarea> </div> </div> </div> </div> <div class="modal-footer"> <div class="row"> <div class="col-md-12"> <input type="submit" class="btn btn-success blocksm" id="voting_Btn" name="voting_Btn" value="ارسال" /> <input type="hidden" name="to_userid" value="' +
          d +
          '" /> <input type="hidden" name="_token" value="' +
          app._token +
          '" /> </div> </div> </div> </form> </div> </div> </div>'
      );
      c.find("#votingFrm")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            message: {
              validators: {
                notEmpty: {
                  message: "نرجو ذكر تجربتك مع البائع مع ذكر السلعة",
                },
                stringLength: { max: 255, message: "اقصي عدد للحروف 255 حرف" },
              },
            },
            advise: {
              validators: {
                notEmpty: {
                  message: "هل تنصح الأعضاء الأخرين بالتعامل مع البائع؟",
                },
              },
            },
          },
        })
        .on("success.form.fv", function (g) {
          g.preventDefault();
          var f = c.find("#advise option:selected").val();
          disableEnable("voting_Btn", 1);
          $.ajax({
            type: "POST",
            url: app.root + "ajax/voting.php?lang=" + app.lang,
            cache: false,
            data: $("#votingFrm").serialize(),
            dataType: "json",
            timeout: TIMEOUTVALUE,
            tryCount: 0,
            retryLimit: 3,
            success: function (h) {
              if (parseInt(h.status) === 1) {
                app.afterVotingSuccessed(f);
                Msg.success(h.txt, MSGBOXTIME);
              } else {
                if (parseInt(h.status) === 0) {
                  Msg.danger(h.txt, MSGBOXTIME);
                } else {
                  Msg.info(h.txt, MSGBOXTIME);
                }
              }
              disableEnable("voting_Btn", 0);
            },
            error: function (h, k, j) {
              if (k === "timeout") {
                this.tryCount++;
                if (this.tryCount <= this.retryLimit) {
                  $.ajax(this);
                  return;
                }
              }
              Msg.danger(ajaxError(k), MSGBOXTIME);
              disableEnable("voting_Btn", 0);
            },
          });
        })
        .on("err.field.fv", function (g, f) {
          if (f.fv.getSubmitButton()) {
            f.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (g, f) {
          if (f.fv.getSubmitButton()) {
            f.fv.disableSubmitButtons(false);
          }
        });
      c.modal("show");
    }
  });
  app.afterVotingSuccessed = function (l) {
    var b = $("#voting"),
      c = b.find(".voting_icon"),
      g = b.find(".voting_text"),
      k = parseInt(b.attr("data-link"));
    if (k == "1") {
      var e = parseInt(b.attr("data-userid")),
        h = b.attr("data-username"),
        f =
          '<a href="' +
          app.base +
          "ratings/" +
          e +
          '/" class="btn btn-success outline-btn blocksm tahoma mrb10" title="تقييمات ' +
          h +
          '"><i class="fa fa-thumbs-o-up"></i> تقييمات العضو';
      $("#link_voting").html(f);
    } else {
      if (l === "1") {
        var d = $("#likeCount"),
          a = parseInt(d.text()) + 1;
        d.text(a);
        b.removeClass("btn-primary outline-btn").addClass(
          "btn-success disabled"
        );
      } else {
        var n = $("#dislikeCount"),
          j = parseInt(n.text()) + 1;
        n.text(j);
        b.removeClass("btn-primary outline-btn").addClass(
          "btn-danger disabled"
        );
        c.removeClass("fa-thumbs-o-up").addClass("fa-thumbs-o-down");
      }
      g.text("تم التقييم");
    }
    $(".removeModal").modal("hide");
  };
  $("#votes").on("click", ".reply_negative_voting", function (a) {
    a.preventDefault();
    if (app.uo === "0" || typeof app.uo === "undefined") {
      w();
    } else {
      var c = $(this),
        b = parseInt(c.attr("data-vid"));
      var d = $(
        '<div class="modal fade replyVoteModal removeModal" id="replyVoteModal" tabindex="-1" role="dialog" aria-labelledby="replyVoteModalLabel" aria-hidden="true"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> <h4 class="modal-title"><i class="fa fa-reply"></i> رد على تقييم</h4> </div> <div class="modal-body"> <div class="row"> <div class="col-md-12"> <form role="form" id="formreplyVote" class="formreplyVote" autocomplete="off" method="POST"> <div class="row"> <div class="col-md-12"> <div class="form-group rel"> <textarea rows="6" cols="95" name="message" id="message" class="form-control input-lg" placeholder="اكتب الرد"></textarea> </div> </div> <div class="col-md-12"> <input type="hidden" name="vid" id="vid" value="' +
          b +
          '"> <input type="hidden" name="_token" value="' +
          app._token +
          '" /> <input type="submit" class="btn btn-success blocksm" id="replyVoteBtn" name="replyVoteBtn" value="ارسال" /> </div> </div> </form> </div> </div> </div> </div> </div> </div>'
      );
      d.find("#formreplyVote")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            message: {
              validators: {
                notEmpty: { message: "الرد مطلوب" },
                stringLength: {
                  min: 6,
                  max: 255,
                  message: "أقل عدد للحروف 6 وأقصى عدد 255 حرف",
                },
              },
            },
          },
        })
        .on("success.form.fv", function (e) {
          e.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "replyVoteBtn",
            sendtype: "POST",
            fullurl: "ajax/reply_vote.php",
            formid: "formreplyVote",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: false,
            redirecturl: null,
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: "removeModal",
          });
        })
        .on("err.field.fv", function (f, e) {
          if (e.fv.getSubmitButton()) {
            e.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (f, e) {
          if (e.fv.getSubmitButton()) {
            e.fv.disableSubmitButtons(false);
          }
        });
      d.modal("show");
    }
  });
  function A(a) {
    var c = $("#messageModal"),
      b = c.find(".smstab");
    b.removeClass("hidden");
    if (a === "1") {
      b.removeClass("hidden");
    } else {
      b.addClass("hidden");
    }
    c.modal("show");
  }
  $(document).on("hide.bs.modal", ".removeModal", function () {
    $(this).fadeOut("slow", function () {
      $(this).remove();
    });
  });
  $("#comments").on("click", ".abuseMessage", function (a) {
    a.preventDefault();
    if (app.uo === "0" || typeof app.uo === "undefined") {
      w();
    } else {
      var b = $(this),
        d = parseInt(b.attr("data-comid")),
        e = parseInt(b.attr("data-comuser"));
      if (e == app.uo) {
        Msg.info("لا تستطيع التبليغ عن تعليقك", MSGBOXTIME);
        return;
      }
      var c = $(
        '<div  id="commentAbuseModal"  class="modal fade commentAbuseModal removeModal" tabindex="-1" role="dialog" aria-labelledby="commentAbuseModalLabel" aria-hidden="true" ><div class="modal-dialog"> <div class="modal-content"><div class="modal-header mrb2"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title" id="commentAbuseModalLabel">بلغ عن هذا التعليق</h4></div><form role="form" id="commentAbuseFrm" class="commentAbuseFrm" method="POST" ><div class="modal-body"><div class="row"><div class="col-md-12"><div class="form-group"><select name="bause_type" id="bause_type" class="form-control input-lg"><option value="">اختر نوع المخالفة</option><option value="1" >تعليق مُخل بالآداب</option><option value="2" >تعليق غير ذو صلة</option><option value="3" >إحتواء التعليق على رسائل دعائية</option><option value="4" >إحتيال أو نصب</option><option value="5" >سبب أخر</option></select></div></div><div class="col-md-12"><div class="form-group"> <textarea class="form-control input-lg" name="message" rows="6" placeholder="تفاصيل البلاغ"></textarea></div></div></div></div><div class="modal-footer"><div class="row"><div class="col-md-12"><input type="submit" class="btn  btn-success blocksm" id="commentAbuseBtn" name="commentAbuseBtn" value="ارسال" /><input type="hidden" name="comment_id" value="' +
          d +
          '" /><input type="hidden" name="_token" value="' +
          app._token +
          '" /></div></div></div></form></div></div></div>'
      );
      c.find("#commentAbuseFrm")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            message: {
              validators: {
                notEmpty: { message: "اكتب المخالفة بالتفاصيل" },
                stringLength: { max: 255, message: "اقصي عدد للحروف 255 حرف" },
              },
            },
            bause_type: { validators: { notEmpty: { message: "اختر السبب" } } },
          },
        })
        .on("success.form.fv", function (f) {
          f.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "commentAbuseBtn",
            sendtype: "POST",
            fullurl: "ajax/comment_abuse.php",
            formid: "commentAbuseFrm",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: false,
            redirecturl: null,
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: "removeModal",
          });
        })
        .on("err.field.fv", function (g, f) {
          if (f.fv.getSubmitButton()) {
            f.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (g, f) {
          if (f.fv.getSubmitButton()) {
            f.fv.disableSubmitButtons(false);
          }
        });
      c.modal("show");
    }
  });
  $("#comments").on("click", ".delComment", function (b) {
    b.preventDefault();
    var d = $(this),
      a = parseInt(d.attr("data-comid")),
      c = $("#commentRow" + a);
    var e = function () {
      $.ajax({
        type: "POST",
        url: app.root + "ajax/comment_del.php?lang=" + app.lang,
        cache: false,
        data: { comid: a, _token: app._token },
        dataType: "json",
        timeout: TIMEOUTVALUE,
        tryCount: 0,
        retryLimit: 3,
        success: function (f) {
          if (parseInt(f.status) === 1) {
            c.remove();
            if ($(".doRefresh").length) {
              if (!$(".actionAfterDel").length) {
                comment("1");
              }
            }
          } else {
            Msg.danger(f.txt, MSGBOXTIME);
          }
        },
        error: function (h, g, f) {
          if (g === "timeout") {
            this.tryCount++;
            if (this.tryCount <= this.retryLimit) {
              $.ajax(this);
              return;
            }
          }
          Msg.danger(ajaxError(g), MSGBOXTIME);
        },
      });
    };
    confirm("حذف التعليق", "هل تريد حذف التعليق ؟", "لا", "نعم", e);
  });
  $(".sendMessageDialog").click(function (a) {
    a.preventDefault();
    if (app.uo === "0" || typeof app.uo === "undefined") {
      w();
    } else {
      var b = $(this),
        d = parseInt(b.attr("data-id")),
        c = b.attr("data-sendmobile");
      $("#sms , #inbox").val("");
      A(c);
      $("#inboxMsg").addClass("in active");
      $("#smsMessage").removeClass("in active");
      $(".smstab").removeClass("active");
      $(".inboxtab").addClass("active");
      $(".adid").val(d);
      $("#sc").val("1");
      $("#smsStatusbar #smsMessageCount , #smsStatusbar #lettersCount").text(
        "0"
      );
    }
  });
  $(".replySMS").click(function (a) {
    a.preventDefault();
    var b = $(this),
      c = parseInt(b.attr("data-id"));
    $("#smsid").val(c);
    $("#sms").val("");
    $("#replysmsModal").modal("show");
    $("#sc").val("1");
    $("#smsStatusbar #smsMessageCount , #smsStatusbar #lettersCount").text("0");
  });
  $(".replyMsg").click(function (a) {
    a.preventDefault();
    var b = $(this),
      c = parseInt(b.attr("data-id"));
    $("#msgid").val(c);
    $("#message").val("");
    $("#replymsgModal").modal("show");
  });
});
if ($("#uploadBtn").length) {
  (function (s, q) {
    var y = s("#uploadBtn"),
      p = s("#customUpload"),
      u = !o() ? "uploadBtn" : "customUpload",
      n = p.attr("data-multi") == "true" ? true : false,
      v = p.attr("data-watermark") === "0" ? "0" : "1",
      w = p.attr("data-checksize") === "0" ? "0" : "1";
    var r = new q.Uploader({
      runtimes: "html5,flash",
      browse_button: "uploadBtn",
      container: document.getElementById(u),
      url: app.root + "_lib/upload_image.php?lang=" + app.lang,
      flash_swf_url: app.root + "style/default/js/plupload/Moxie.swf",
      chunk_size: "1mb",
      resize: { width: 800, height: 600, quality: 100 },
      multi_selection: n,
      filters: {
        max_file_size: "3mb",
        mime_types: [{ title: "Image files", extensions: "jpg,gif,png,jpeg" }],
      },
      multipart_params: { watemark: v, checksize: w },
    });
    r.init();
    r.bind("PostInit", function (b, a) {});
    r.bind("UploadProgress", function (c, b) {
      var a = b.percent;
      s("#" + b.id)
        .find(".progress-bar")
        .css("width", a + "%")
        .attr("aria-valuenow", a)
        .text(a + "%");
    });
    r.bind("Error", function (d, a) {
      var b,
        c = a.file;
      switch (a.code) {
        case q.FILE_SIZE_ERROR:
          b = "حجم الملف " + c.name + " كبير جدا";
          break;
        case q.FILE_EXTENSION_ERROR:
          b = "امتداد الملف " + c.name + " غير مسموح";
          break;
        default:
          b = "هناك خطأ أعد المحاولة";
          break;
      }
      s("#" + c.id).remove();
      d.removeFile(c);
      Msg.danger(b, MSGBOXTIME);
      r.refresh();
    });
    r.bind("FileUploaded", function (f, d, e) {
      var a = s("#" + d.id);
      if (d.status == q.DONE) {
        var c =
            "<div class='icon'><i class='fa fa-check-circle success-upload-icon'></i></div>",
          b = JSON.parse(e.response);
        a.find(".progress").remove();
        if (b.error) {
          Msg.danger(b.message, MSGBOXTIME);
          a.remove();
        } else {
          a.find("img").removeClass("opacity");
          s(c).prependTo(a);
          x(d.id, q.xmlEncode(b.message));
        }
      } else {
        s("#" + d.id).remove();
        f.removeFile(d);
      }
    });
    r.bind("QueueChanged", function (a) {});
    r.bind("BeforeUpload", function (b, a) {});
    r.bind("UploadComplete", function () {});
    r.bind("FilesAdded", function (b, a) {
      s.each(a, function (c, d) {
        t(d);
        s("#" + d.id + " .delete").click(function () {
          s("#" + d.id).remove();
          var e = d.status;
          b.removeFile(d);
          if (b.state == q.STARTED && e == q.UPLOADING) {
            b.stop();
            b.start();
          }
        });
      });
      r.start();
      r.refresh();
    });
    function t(b) {
      var c =
        "<li id='" +
        b.id +
        "'><div class='action'><i class='fa fa-2x fa-minus-circle delete' title='حذف'></i></div><div class='progress'><div class='progress-bar progress-bar-striped active' role='progressbar'   aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:0%'></div></div></li>";
      s(y).before(c);
      var a = s(new Image())
        .appendTo(s("#" + b.id))
        .addClass("opacity");
      var d = new mOxie.Image();
      d.onload = function () {
        d.downsize(300, 300);
        a.prop("src", d.getAsDataURL());
      };
      d.onerror = function () {
        a.attr("src", app.root + "images/no-image.png");
      };
      d.load(b.getSource());
    }
    function o() {
      var a = document.createElement("canvas");
      return !!(a.getContext && a.getContext("2d"));
    }
    function x(a, c) {
      if (!s("#" + a).length) {
        return false;
      }
      var b =
        '<input class="uplpadedphoto"  type="hidden" name="images[]" id="photo_' +
        a +
        '" value="' +
        c +
        '" />';
      s(b).appendTo(s("#" + a));
    }
  })(jQuery, plupload);
}
if ($("#uploadBtnFiles").length) {
  (function (s, q) {
    var y = s("#uploadBtnFiles"),
      p = s("#customUploadFiles"),
      u = !o() ? "uploadBtnFiles" : "customUploadFiles",
      n = true,
      v = "0",
      w = p.attr("data-checksize") === "0" ? "0" : "1";
    var r = new q.Uploader({
      runtimes: "html5,flash",
      browse_button: "uploadBtnFiles",
      container: document.getElementById(u),
      url: app.root + "_lib/upload_files.php?lang=" + app.lang,
      flash_swf_url: app.root + "style/default/js/plupload/Moxie.swf",
      chunk_size: "1mb",
      multi_selection: n,
      filters: {
        max_file_size: "20mb",
        mime_types: [
          { title: "Image files", extensions: "jpg,gif,png,jpeg,doc,pdf" },
        ],
      },
      multipart_params: { checksize: w },
    });
    r.init();
    r.bind("PostInit", function (b, a) {});
    r.bind("UploadProgress", function (c, b) {
      var a = b.percent;
      s("#" + b.id)
        .find(".progress-bar")
        .css("width", a + "%")
        .attr("aria-valuenow", a)
        .text(a + "%");
    });
    r.bind("Error", function (d, a) {
      var b,
        c = a.file;
      switch (a.code) {
        case q.FILE_SIZE_ERROR:
          b = "حجم الملف " + c.name + " كبير جدا";
          break;
        case q.FILE_EXTENSION_ERROR:
          b = "امتداد الملف " + c.name + " غير مسموح";
          break;
        default:
          b = "هناك خطأ أعد المحاولة";
          break;
      }
      s("#" + c.id).remove();
      d.removeFile(c);
      Msg.danger(b, MSGBOXTIME);
      r.refresh();
    });
    r.bind("FileUploaded", function (f, d, e) {
      var a = s("#" + d.id);
      if (d.status == q.DONE) {
        var c =
            "<div class='icon'><i class='fa fa-check-circle success-upload-icon'></i></div>",
          b = JSON.parse(e.response);
        a.find(".progress").remove();
        if (b.error) {
          Msg.danger(b.message, MSGBOXTIME);
          a.remove();
        } else {
          a.find("img").removeClass("opacity");
          s(c).prependTo(a);
          x(d.id, q.xmlEncode(b.message));
        }
      } else {
        s("#" + d.id).remove();
        f.removeFile(d);
      }
    });
    r.bind("QueueChanged", function (a) {});
    r.bind("BeforeUpload", function (b, a) {});
    r.bind("UploadComplete", function () {});
    r.bind("FilesAdded", function (b, a) {
      s.each(a, function (c, d) {
        t(d);
        s("#" + d.id + " .delete").click(function () {
          s("#" + d.id).remove();
          var e = d.status;
          b.removeFile(d);
          if (b.state == q.STARTED && e == q.UPLOADING) {
            b.stop();
            b.start();
          }
        });
      });
      r.start();
      r.refresh();
    });
    function t(b) {
      var c =
        "<li id='" +
        b.id +
        "'><div class='action'><i class='fa fa-2x fa-minus-circle delete' title='حذف'></i></div><div class='progress'><div class='progress-bar progress-bar-striped active' role='progressbar'   aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:0%'></div></div></li>";
      s(y).before(c);
      var a = s(new Image())
        .appendTo(s("#" + b.id))
        .addClass("opacity");
      var d = new mOxie.Image();
      d.onload = function () {
        d.downsize(300, 300);
        a.prop("src", d.getAsDataURL());
      };
      d.onerror = function () {
        a.attr("src", app.root + "images/no-image.png");
      };
      d.load(b.getSource());
    }
    function o() {
      var a = document.createElement("canvas");
      return !!(a.getContext && a.getContext("2d"));
    }
    function x(a, c) {
      if (!s("#" + a).length) {
        return false;
      }
      var b =
        '<input class="uplpadedphoto"  type="hidden" name="images[]" id="photo_' +
        a +
        '" value="' +
        c +
        '" />';
      s(b).appendTo(s("#" + a));
    }
  })(jQuery, plupload);
}
function pageLoaded() {
  $(".spinnerLoading").hide();
}
function fireWhenReady() {
  if (typeof pageLoaded === "function") {
    pageLoaded();
  } else {
    setTimeout(fireWhenReady, 1000);
  }
}
