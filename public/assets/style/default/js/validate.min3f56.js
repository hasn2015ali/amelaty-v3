$(document).ready(function () {
    if ($("#register-mobile-form").length) {
      $("#register-mobile-form")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          addOns: {
            reCaptcha2: {
              element: "captchaContainerMobile",
              theme: "light",
              siteKey: app.sitekey,
              language: "ar",
              timeout: 160,
              message: " أنا لست برنامج روبوت مطلوب",
            },
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            "g-recaptcha-response": { err: "#captchaMessageMobile" },
            name: { validators: { notEmpty: { message: "الإسم مطلوب" } } },
            account_type: {
              validators: { notEmpty: { message: "اختر نوع الحساب" } },
            },
            mobile: {
              validators: {
                notEmpty: { message: "رقم الجوال مطلوب" },
                stringLength: {
                  min: 6,
                  max: 27,
                  message: "أقل عدد للأرقام 6 وأكبر عدد 27 رقم",
                },
                numeric: { message: "رقم الجوال أرقام فقط" },
                callback: {
                  message: "أدخل رقم الجوال بدون الكود الدولي",
                  callback: function (a) {
                    return internationalCode(a, "dailcode");
                  },
                },
              },
            },
            regpassword: {
              validators: {
                notEmpty: { message: "كلمة المرور مطلوبة" },
                stringLength: {
                  min: 6,
                  max: 30,
                  message: "أقل عدد للحروف 6 وأقصى عدد 30 حرف",
                },
              },
            },
            storename: {
              validators: {
                notEmpty: { message: "اسم مكتبك مطلوب" },
                stringLength: {
                  min: 10,
                  max: 150,
                  message: "أقل عدد للحروف 10 وأقصى عدد 150 حرف",
                },
              },
            },
            areaid: { validators: { notEmpty: { message: "اختر المدينة" } } },
            "catid[]": {
              validators: { notEmpty: { message: "اختر مجال نشاطك" } },
            },
            details: {
              validators: {
                notEmpty: { message: "اكتب مزيد من التفاصيل عن مكتبك" },
                stringLength: {
                  min: 30,
                  max: 255,
                  message: "أقل عدد للحروف 30 وأقصى عدد 255 حرف",
                },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "registerMobileBtn",
            sendtype: "POST",
            fullurl: "ajax/register_mobile.php",
            formid: "register-mobile-form",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: true,
            containerid: "register-container",
            redirect: true,
            redirecturl: app.url + "account-active/",
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: false,
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#register-social-form").length) {
      $("#register-social-form")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            name: { validators: { notEmpty: { message: "الإسم مطلوب" } } },
            regemail: {
              validators: {
                notEmpty: { message: "البريد الإلكتروني مطلوب" },
                emailAddress: { message: "البريد الإلكتروني غير صحيح" },
                remote: {
                  type: "POST",
                  url: app.root + "ajax/checkEmailValid.php?lang=" + app.lang,
                  delay: 2000,
                },
              },
            },
            mobile: {
              validators: {
                notEmpty: { message: "رقم الجوال مطلوب" },
                stringLength: {
                  min: 6,
                  max: 27,
                  message: "أقل عدد للأرقام 6 وأكبر عدد 27 رقم",
                },
                numeric: { message: "رقم الجوال أرقام فقط" },
                callback: {
                  message: "أدخل رقم الجوال بدون الكود الدولي",
                  callback: function (a) {
                    return internationalCode(a, "dailcode");
                  },
                },
              },
            },
            regpassword: {
              validators: {
                notEmpty: { message: "كلمة المرور مطلوبة" },
                stringLength: {
                  min: 6,
                  max: 30,
                  message: "أقل عدد للحروف 6 وأقصى عدد 30 حرف",
                },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "registerSocialBtn",
            sendtype: "POST",
            fullurl: "ajax/register_social.php",
            formid: "register-social-form",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: true,
            containerid: "register-container",
            redirect: true,
            redirecturl: app.url + "account-active/",
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: false,
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#login-form").length) {
      var h = $("#login-form");
      h.formValidation({
        framework: "bootstrap",
        err: { container: "tooltip" },
        icon: {
          valid: "fa fa-check",
          invalid: "fa fa-times",
          validating: "fa fa-refresh",
        },
        fields: {
          mobile: {
            validators: {
              notEmpty: { message: "رقم الجوال مطلوب" },
              stringLength: {
                min: 6,
                max: 27,
                message: "أقل عدد للأرقام 6 وأكبر عدد 27 رقم",
              },
              numeric: { message: "رقم الجوال أرقام فقط" },
              callback: {
                message: "أدخل رقم الجوال بدون الكود الدولي",
                callback: function (a) {
                  return internationalCode(a, "dailcodeLogin");
                },
              },
            },
          },
          regpassword: {
            validators: {
              notEmpty: { message: "كلمة المرور مطلوبة" },
              stringLength: {
                min: 6,
                max: 30,
                message: "أقل عدد للحروف 6 وأقصى عدد 30 حرف",
              },
            },
          },
        },
      })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          var b = h.attr("data-modal") === "1" ? "loginModal" : false;
          callAjx({
            loadingicon: false,
            sendBtn: "loginBtn",
            sendtype: "POST",
            fullurl: "ajax/login.php",
            formid: "login-form",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: "login-form",
            redirect: b === false ? true : false,
            redirecturl: app.url + "account-active/",
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: "تم تسجيل الدخول",
            calltype: b,
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#place-ad-form").length) {
      $("#place-ad-form")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          addOns: {
            reCaptcha2: {
              element: "captchaContainer",
              theme: "light",
              siteKey: app.sitekey,
              language: "ar",
              timeout: 160,
              message: " أنا لست برنامج روبوت مطلوب",
            },
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            "g-recaptcha-response": { err: "#captchaMessage" },
            title: {
              validators: {
                notEmpty: { message: "موضوع الإعلان مطلوب" },
                stringLength: { max: 30, message: "أقصى عدد للحروف 30 حرف" },
                callback: {
                  message: "حروف عربي وانجليزي فقط",
                  callback: function (a) {
                    return ar_en_letters_only(a);
                  },
                },
              },
            },
            catid: { validators: { notEmpty: { message: "اختر  فئة الإعلان" } } },
            avatar_id: {
              validators: { notEmpty: { message: "اختر  صورة الإعلان" } },
            },
            areaid: { validators: { notEmpty: { message: "اختر  المدينة" } } },
            nationality: {
              validators: { notEmpty: { message: "جنسية العاملة مطلوبة" } },
            },
            adtype: { validators: { notEmpty: { message: "اختر نوع الإعلان" } } },
            details: {
              validators: { notEmpty: { message: "اكتب تفاصيل الإعلان" } },
            },
            name: {
              validators: {
                notEmpty: { message: "الإسم مطلوب" },
                stringLength: { max: 50, message: "أقصى عدد للحروف 50 حرف" },
              },
            },
            regemail: {
              validators: {
                emailAddress: { message: "البريد الإلكتروني غير صحيح" },
              },
            },
            mobile: {
              validators: {
                notEmpty: { message: "رقم الجوال مطلوب" },
                stringLength: {
                  min: 6,
                  max: 27,
                  message: "أقل عدد للأرقام 6 وأكبر عدد 27 رقم",
                },
                numeric: { message: "رقم الجوال أرقام فقط" },
                callback: {
                  message: "أدخل رقم الجوال بدون الكود الدولي",
                  callback: function (a) {
                    return internationalCode(a, "dailcode");
                  },
                },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "placeadBtn",
            sendtype: "POST",
            fullurl: "ajax/place_ad.php",
            formid: "place-ad-form",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: true,
            containerid: "place-ad-container",
            redirect: false,
            redirecturl: null,
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: false,
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#new-ad-form").length) {
      var e = $("#new-ad-form"),
        f = "my-adverts/";
      e.formValidation({
        framework: "bootstrap",
        err: { container: "tooltip" },
        icon: {
          valid: "fa fa-check",
          invalid: "fa fa-times",
          validating: "fa fa-refresh",
        },
        excluded: [":disabled", ":hidden", ":not(:visible)"],
        fields: {
          title: {
            validators: {
              notEmpty: { message: "موضوع الإعلان مطلوب" },
              stringLength: { max: 30, message: "أقصى عدد للحروف 30 حرف" },
              callback: {
                message: "حروف عربي وانجليزي فقط",
                callback: function (a) {
                  return ar_en_letters_only(a);
                },
              },
            },
          },
          catid: { validators: { notEmpty: { message: "اختر  فئة الإعلان" } } },
          avatar_id: {
            validators: { notEmpty: { message: "اختر  صورة الإعلان" } },
          },
          areaid: { validators: { notEmpty: { message: "اختر  المدينة" } } },
          nationality: {
            validators: { notEmpty: { message: "جنسية العاملة مطلوبة" } },
          },
          adtype: { validators: { notEmpty: { message: "اختر نوع الإعلان" } } },
          details: {
            validators: { notEmpty: { message: "اكتب تفاصيل الإعلان" } },
          },
        },
      })
        .on("success.form.fv", function (b) {
          b.preventDefault();
          var a =
            $("#newadBtn").attr("data-pr") == "1" ? null : "showpopUpPremium";
          callAjx({
            loadingicon: false,
            sendBtn: "newadBtn",
            sendtype: "POST",
            fullurl: "ajax/new_ad.php",
            formid: "new-ad-form",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: true,
            containerid: "adding_ad_msg",
            redirect: false,
            redirecturl: null,
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: a,
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
      if ($(".subfields").length) {
        var g = $(".subfields");
        if (g.attr("data-trigger") == "1") {
          g.trigger("change");
        }
      }
    }
    if ($("#contactus-form").length) {
      $("#contactus-form")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          addOns: {
            reCaptcha2: {
              element: "captchaContainer",
              theme: "light",
              siteKey: app.sitekey,
              language: "ar",
              timeout: 160,
              message: " أنا لست برنامج روبوت مطلوب",
            },
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            "g-recaptcha-response": { err: "#captchaMessage" },
            name: {
              validators: {
                notEmpty: { message: "اكتب أسمك" },
                stringLength: { max: 150, message: "أقصى عدد للحروف 150 حرف" },
              },
            },
            areaid: { validators: { notEmpty: { message: "اختر المدينة" } } },
            email: {
              validators: {
                notEmpty: { message: "البريد الإلكتروني مطلوب" },
                emailAddress: { message: "البريد الإلكتروني غير صحيح" },
              },
            },
            mobile: {
              validators: {
                notEmpty: { message: "رقم الجوال مطلوب" },
                stringLength: {
                  min: 6,
                  max: 27,
                  message: "أقل عدد للأرقام 6 وأكبر عدد 27 رقم",
                },
                numeric: { message: "رقم الجوال أرقام فقط" },
                callback: {
                  message: "أدخل رقم الجوال بدون الكود الدولي",
                  callback: function (a) {
                    return internationalCode(a, "dailcode");
                  },
                },
              },
            },
            why: { validators: { notEmpty: { message: "اختر سبب الأتصال" } } },
            message: {
              validators: {
                notEmpty: { message: "اكتب  الرسالة" },
                stringLength: { min: 5, message: "الرسالة قصيرة جدا" },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "contactusBtn",
            sendtype: "POST",
            fullurl: "ajax/contactus.php",
            formid: "contactus-form",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: true,
            containerid: "contactusContainer",
            redirect: false,
            redirecturl: null,
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: false,
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#formInboxMsg").length) {
      $("#formInboxMsg")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          fields: {
            inbox: {
              validators: {
                notEmpty: { message: "اكتب  الرسالة" },
                stringLength: { min: 5, message: "الرسالة قصيرة جدا" },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "inboxMsgBtn",
            sendtype: "POST",
            fullurl: "ajax/sendinbox.php",
            formid: "formInboxMsg",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: false,
            redirecturl: null,
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: "afterSendInbox",
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#formReplyMsgMessage").length) {
      $("#formReplyMsgMessage")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          fields: {
            message: {
              validators: {
                notEmpty: { message: "اكتب  الرسالة" },
                stringLength: { min: 5, message: "الرسالة قصيرة جدا" },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "replyMsgBtn",
            sendtype: "POST",
            fullurl: "ajax/replymsg.php",
            formid: "formReplyMsgMessage",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: false,
            redirecturl: null,
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: "afterSendReplyMsg",
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#formReplySmsMessage").length) {
      $("#formReplySmsMessage")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          fields: {
            sms: {
              validators: {
                notEmpty: { message: "الرسالة مطلوبة" },
                stringLength: {
                  min: 10,
                  max: 255,
                  message: "أقل عدد للحروف 10 وأقصى عدد 255 حرف",
                },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          smsInfo();
          callAjx({
            loadingicon: false,
            sendBtn: "replySMSBtn",
            sendtype: "POST",
            fullurl: "ajax/replysms.php",
            formid: "formReplySmsMessage",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: false,
            redirecturl: null,
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: "afterSendReplySMS",
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#formSmsMessage").length) {
      $("#formSmsMessage")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          fields: {
            sms: {
              validators: {
                notEmpty: { message: "الرسالة مطلوبة" },
                stringLength: {
                  min: 10,
                  max: 255,
                  message: "أقل عدد للحروف 10 وأقصى عدد 255 حرف",
                },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          smsInfo();
          callAjx({
            loadingicon: false,
            sendBtn: "smsMessageBtn",
            sendtype: "POST",
            fullurl: "ajax/sendsms.php",
            formid: "formSmsMessage",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: false,
            redirecturl: null,
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: "afterSendSMS",
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#form-comment").length) {
      $("#form-comment")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          addOns: {
            reCaptcha2: {
              element: "captchaContainer",
              theme: "light",
              siteKey: app.sitekey,
              language: "ar",
              timeout: 160,
              message: " أنا لست برنامج روبوت مطلوب",
            },
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            "g-recaptcha-response": { err: "#captchaMessage" },
            commentdata: {
              validators: {
                notEmpty: { message: "التعليق مطلوب" },
                stringLength: { min: 5, message: "التعليق قصير جدا" },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          addComment(false);
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#restPasswordForm").length) {
      $("#restPasswordForm")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          fields: {
            regpassword: {
              validators: {
                notEmpty: { message: "كلمة المرور مطلوبة" },
                stringLength: {
                  min: 6,
                  max: 30,
                  message: "أقل عدد للحروف 6 وأقصى عدد 30 حرف",
                },
              },
            },
            confirmregpassword: {
              validators: {
                notEmpty: { message: "كلمة المرور مطلوبة" },
                identical: {
                  field: "regpassword",
                  message: "كلمة المرور غير متطابقة",
                },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "restPasswordBtn",
            sendtype: "POST",
            fullurl: "ajax/rest_password.php",
            formid: "restPasswordForm",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: true,
            redirecturl: app.url + "login/",
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: false,
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#changePasswordForm").length) {
      $("#changePasswordForm")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          fields: {
            name: {
              validators: { notEmpty: { message: "الإسم بالكامل مطلوب" } },
            },
            regpassword: {
              validators: {
                notEmpty: { message: "كلمة المرور مطلوبة" },
                stringLength: {
                  min: 6,
                  max: 30,
                  message: "أقل عدد للحروف 6 وأقصى عدد 30 حرف",
                },
              },
            },
            confirmregpassword: {
              validators: {
                notEmpty: { message: "كلمة المرور مطلوبة" },
                identical: {
                  field: "regpassword",
                  message: "كلمة المرور غير متطابقة",
                },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "changePasswordBtn",
            sendtype: "POST",
            fullurl: "ajax/change_password.php",
            formid: "changePasswordForm",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: true,
            redirecturl: app.url + "my-account/",
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: false,
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#changeEmailForm").length) {
      $("#changeEmailForm")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          fields: {
            regemail: {
              validators: {
                notEmpty: { message: "البريد الإلكتروني مطلوب" },
                emailAddress: { message: "البريد الإلكتروني غير صحيح" },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "changeEmailBtn",
            sendtype: "POST",
            fullurl: "ajax/change_email.php",
            formid: "changeEmailForm",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: true,
            redirecturl: app.url + "my-account/",
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: false,
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#changeMobileForm").length) {
      $("#changeMobileForm")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          fields: {
            mobile: {
              validators: {
                notEmpty: { message: "رقم الجوال مطلوب" },
                stringLength: {
                  min: 6,
                  max: 27,
                  message: "أقل عدد للأرقام 6 وأكبر عدد 27 رقم",
                },
                numeric: { message: "رقم الجوال أرقام فقط" },
                callback: {
                  message: "أدخل رقم الجوال بدون الكود الدولي",
                  callback: function (a) {
                    return internationalCode(a, "dailcode");
                  },
                },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "changeMobileBtn",
            sendtype: "POST",
            fullurl: "ajax/change_mobile.php",
            formid: "changeMobileForm",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: true,
            redirecturl: app.url + "my-account/",
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: false,
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#edit-ad-form").length) {
      $("#edit-ad-form")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            title: {
              validators: {
                notEmpty: { message: "موضوع الإعلان مطلوب" },
                stringLength: { max: 30, message: "أقصى عدد للحروف 30 حرف" },
                callback: {
                  message: "حروف عربي وانجليزي فقط",
                  callback: function (a) {
                    return ar_en_letters_only(a);
                  },
                },
              },
            },
            catid: { validators: { notEmpty: { message: "اختر  فئة الإعلان" } } },
            areaid: { validators: { notEmpty: { message: "اختر  المدينة" } } },
            sub1: { validators: { notEmpty: { message: "مطلوب" } } },
            sub2: { validators: { notEmpty: { message: "مطلوب" } } },
            sub3: { validators: { notEmpty: { message: "مطلوب" } } },
            sub4: { validators: { notEmpty: { message: "مطلوب" } } },
            nationality: {
              validators: { notEmpty: { message: "جنسية العاملة مطلوبة" } },
            },
            adtype: { validators: { notEmpty: { message: "اختر نوع الإعلان" } } },
            details: {
              validators: { notEmpty: { message: "اكتب تفاصيل الإعلان" } },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "editadBtn",
            sendtype: "POST",
            fullurl: "ajax/edit_ad.php",
            formid: "edit-ad-form",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: true,
            redirecturl: app.url + "my-adverts/",
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: false,
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#addStoreForm").length) {
      $("#addStoreForm")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            storename: {
              validators: {
                notEmpty: { message: "اسم مكتبك مطلوب" },
                stringLength: {
                  min: 10,
                  max: 150,
                  message: "أقل عدد للحروف 10 وأقصى عدد 150 حرف",
                },
              },
            },
            areaid: { validators: { notEmpty: { message: "اختر المدينة" } } },
            "catid[]": {
              validators: { notEmpty: { message: "اختر مجال نشاطك" } },
            },
            details: {
              validators: {
                notEmpty: { message: "اكتب مزيد من التفاصيل عن مكتبك" },
                stringLength: {
                  min: 30,
                  max: 255,
                  message: "أقل عدد للحروف 30 وأقصى عدد 255 حرف",
                },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "addStoreBtn",
            sendtype: "POST",
            fullurl: "ajax/store_add.php",
            formid: "addStoreForm",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: true,
            redirecturl: app.url + "my-office/",
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: false,
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#editStoreForm").length) {
      $("#editStoreForm")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            storename: {
              validators: {
                notEmpty: { message: "اسم مكتبك مطلوب" },
                stringLength: {
                  min: 10,
                  max: 150,
                  message: "أقل عدد للحروف 10 وأقصى عدد 150 حرف",
                },
              },
            },
            areaid: { validators: { notEmpty: { message: "اختر المدينة" } } },
            "catid[]": {
              validators: { notEmpty: { message: "اختر مجال نشاطك" } },
            },
            details: {
              validators: {
                notEmpty: { message: "اكتب مزيد من التفاصيل عن مكتبك" },
                stringLength: {
                  min: 30,
                  max: 255,
                  message: "أقل عدد للحروف 30 وأقصى عدد 255 حرف",
                },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "editStoreBtn",
            sendtype: "POST",
            fullurl: "ajax/store_edit.php",
            formid: "editStoreForm",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: true,
            redirecturl: app.url + "my-office/",
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: false,
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#byemail-form").length) {
      $("#byemail-form")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          addOns: {
            reCaptcha2: {
              element: "captchaContainerEmail",
              theme: "light",
              siteKey: app.sitekey,
              language: "ar",
              timeout: 160,
              message: " أنا لست برنامج روبوت مطلوب",
            },
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            "g-recaptcha-response": { err: "#captchaMessageEmail" },
            resemail: {
              validators: {
                notEmpty: { message: "البريد الإلكتروني مطلوب" },
                emailAddress: { message: "البريد الإلكتروني غير صحيح" },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "byemailBtn",
            sendtype: "POST",
            fullurl: "ajax/restorepasswordbyemail.php",
            formid: "byemail-form",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: false,
            redirecturl: null,
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: false,
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#bysms-form").length) {
      $("#bysms-form")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          addOns: {
            reCaptcha2: {
              element: "captchaContainerMobile",
              theme: "light",
              siteKey: app.sitekey,
              language: "ar",
              timeout: 160,
              message: " أنا لست برنامج روبوت مطلوب",
            },
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            "g-recaptcha-response": { err: "#captchaMessageMobile" },
            mobile: {
              validators: {
                notEmpty: { message: "رقم الجوال مطلوب" },
                stringLength: {
                  min: 6,
                  max: 27,
                  message: "أقل عدد للأرقام 6 وأكبر عدد 27 رقم",
                },
                numeric: { message: "رقم الجوال أرقام فقط" },
                callback: {
                  message: "أدخل رقم الجوال بدون الكود الدولي",
                  callback: function (a) {
                    return internationalCode(a, "dailcode");
                  },
                },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "bysmsBtn",
            sendtype: "POST",
            fullurl: "ajax/restorepasswordsms.php",
            formid: "bysms-form",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: true,
            redirecturl: app.url,
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: false,
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#transfer-form").length) {
      $("#transfer-form")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            moneysender: {
              validators: {
                notEmpty: { message: "أسم المحول مطلوب" },
                stringLength: { max: 200, message: "أقصى عدد للحروف 200 حرف" },
              },
            },
            amount: {
              validators: {
                notEmpty: { message: "المبلغ المحول مطلوب" },
                stringLength: { min: 1, message: "المبلغ المحول مطلوب" },
                numeric: { message: "أرقام فقط" },
              },
            },
            bank_id: {
              validators: {
                notEmpty: { message: "اختر البنك الذي تم التحويل إليه" },
              },
            },
            datetransfer: {
              validators: { notEmpty: { message: "اختر متي تم التحويل؟" } },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "transferBtn",
            sendtype: "POST",
            fullurl: "ajax/confirm_transfer.php",
            formid: "transfer-form",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: true,
            containerid: "transferContainer",
            redirect: false,
            redirecturl: null,
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: false,
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#blacklist-form").length) {
      $("#blacklist-form")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            bank_mobile: {
              validators: {
                notEmpty: { message: "أدخل رقم الحساب أو رقم الجوال" },
                stringLength: { max: 150, message: "أقصى عدد للحروف 150 حرف" },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "blacklistBtn",
            sendtype: "POST",
            fullurl: "ajax/check_blacklist.php",
            formid: "blacklist-form",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: false,
            redirecturl: null,
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: false,
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#surveyForm").length) {
      $("#surveyForm")
        .formValidation()
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "surveyBtn",
            sendtype: "POST",
            fullurl: "ajax/add_survey.php",
            formid: "surveyForm",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: true,
            containerid: "surveyContainer",
            redirect: false,
            redirecturl: null,
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: false,
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#active-mobile-form").length) {
      $("#active-mobile-form")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            mobile_active_code: {
              validators: {
                notEmpty: { message: "كود التفعيل مطلوب" },
                numeric: { message: "أرقام فقط" },
                stringLength: { min: 5, max: 5, message: "كود غير صحيح" },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "activeMobileBtn",
            sendtype: "POST",
            fullurl: "ajax/active_mobile.php",
            formid: "active-mobile-form",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: false,
            redirecturl: null,
            modal: false,
            msgalert: true,
            disablesuccessBtn: true,
            disablesuccessBtntxt: null,
            calltype: "mobileAfterActivted",
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
    if ($("#resend-mobile-code-form").length) {
      $("#resend-mobile-code-form")
        .formValidation({
          framework: "bootstrap",
          err: { container: "tooltip" },
          icon: {
            valid: "fa fa-check",
            invalid: "fa fa-times",
            validating: "fa fa-refresh",
          },
          excluded: [":disabled", ":hidden", ":not(:visible)"],
          fields: {
            mobile: {
              validators: {
                notEmpty: { message: "رقم الجوال مطلوب" },
                stringLength: {
                  min: 6,
                  max: 27,
                  message: "أقل عدد للأرقام 6 وأكبر عدد 27 رقم",
                },
                numeric: { message: "رقم الجوال أرقام فقط" },
                callback: {
                  message: "أدخل رقم الجوال بدون الكود الدولي",
                  callback: function (a) {
                    return internationalCode(a, "dailcode");
                  },
                },
              },
            },
          },
        })
        .on("success.form.fv", function (a) {
          a.preventDefault();
          callAjx({
            loadingicon: false,
            sendBtn: "resendMobileCode",
            sendtype: "POST",
            fullurl: "ajax/resend_mobile_code.php",
            formid: "resend-mobile-code-form",
            datetype: "json",
            resdiv: null,
            cached: false,
            incontainer: false,
            containerid: null,
            redirect: false,
            redirecturl: null,
            modal: false,
            msgalert: true,
            disablesuccessBtn: false,
            disablesuccessBtntxt: null,
            calltype: "showMobileActiveForm",
          });
        })
        .on("err.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        })
        .on("success.field.fv", function (a, b) {
          if (b.fv.getSubmitButton()) {
            b.fv.disableSubmitButtons(false);
          }
        });
    }
  });
  