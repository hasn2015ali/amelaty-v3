<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->id();
            $table->string("title");
            $table->unsignedInteger("cat_id");
            $table->unsignedInteger("city_id");
            $table->unsignedInteger("user_id");
            $table->text("details")->nullable();
            $table->string("image")->nullable();
            $table->unsignedBigInteger("tag_id");
            $table->integer("is_fixed")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
