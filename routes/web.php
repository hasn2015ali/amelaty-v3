<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/register', [UserController::class, 'registerForm']);
Route::get('/login', [UserController::class, 'loginForm']);
Route::get('/restore-password-form', [UserController::class, 'restorePasswordForm']);


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
