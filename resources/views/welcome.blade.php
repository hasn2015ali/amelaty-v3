<!DOCTYPE html>
<html lang="ar" prefix="og: http://ogp.me/ns#">

<head>
    <meta charset="utf-8">
    <title>موقع خدمكم للعمالة المنزلية في السعودية</title>
    <meta name="description"
        content="خدمكم للعمالة المنزلية في السعودية استقدام وتنازل خادمات تأجير بالساعة باليوم بالشهر من جميع الدول الفلبين المغرب الهند بنجلاديش أوغندا.">
    <meta name="Keywords"
        content="خدمكم,عمالة منزلية في السعودية ,خادمات للتنازل, خادمات بالساعة,خادمات بالشهر, استقدام خادمات, تأجير خادمات, خادمات للتنازل, عاملات بالساعة,عاملات بالشهر,خادمات فلبينيات, خادمات مغربيات, خادمات أوغندا, خادمات بنجلاديش, شركات تأجير خادمات بالشهر">
    <meta http-equiv="content-language" content="ar-sa">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-dns-prefetch-control" content="on">
    <meta name="ROBOTS" content="INDEX, FOLLOW">
    <link rel="dns-prefetch" href="http://www.googletagmanager.com/">
    <link rel="dns-prefetch" href="http://www.google-analytics.com/">
    <link rel="dns-prefetch" href="http://ssl.google-analytics.com/">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="موقع خدمكم للعمالة المنزلية في السعودية" />
    <meta property="og:site_name" content="موقع خدمكم للعمالة المنزلية في السعودية" />
    <meta property="og:description"
        content="خدمكم للعمالة المنزلية في السعودية استقدام وتنازل خادمات تأجير بالساعة باليوم بالشهر من جميع الدول الفلبين المغرب الهند بنجلاديش أوغندا." />
    <meta property="og:keyword"
        content="خدمكم,عمالة منزلية في السعودية ,خادمات للتنازل, خادمات بالساعة,خادمات بالشهر, استقدام خادمات, تأجير خادمات, خادمات للتنازل, عاملات بالساعة,عاملات بالشهر,خادمات فلبينيات, خادمات مغربيات, خادمات أوغندا, خادمات بنجلاديش, شركات تأجير خادمات بالشهر" />
    <meta name="REVISIT-AFTER" content="1 DAYS">
    <meta name="RATING" content="GENERAL">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <meta name="author" content="websecretsgroup.com">
    <link rel="manifest" href="manifest.json">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="white">
    <meta name="apple-mobile-web-app-title" content="خدمكم">
    <meta name="application-name" content="خدمكم">
    <link rel="apple-touch-icon" href="assets/icon/icon-57x57.png" sizes="57x57">
    <link rel="apple-touch-icon" href="assets/icon/icon-180x180.png" sizes="180x180">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/icon/icon-32x32.png">
    <link rel="icon" type="image/png" sizes="194x194" href="assets/icon/icon-194x194.png">
    <link rel="icon" type="image/png" sizes="192x192" href="assets/icon/icon-192x192.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/icon/icon-16x16.png">
    <link rel="mask-icon" href="assets/icon/safari-pinned-tab.svg" color="#4091d2">
    <meta name="msapplication-TileImage" content="assets/icon/icon-144x144.png">
    <meta name="msapplication-TileColor" content="#fff">
    <meta name="msapplication-config" content="assets/icon/browserconfig.xml">
    <meta name="theme-color" content="#4091d2">
    <link rel="shortcut icon" href="assets/icon/favicon.ico">
    <link rel="stylesheet" href="{{ asset('assets/style/default/css/bootstrap-ar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/style/default/fonts/font-awesome-4.4.0/css/font-awesome.min.css') }}">
    <link href="{{ asset('assets/style/default/css/styles.min3f56.css') }}?v=11" rel="stylesheet">

    <style>
        #website-logo a {
            background: none;
        }

    </style>
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-P5JMM67');
    </script>
    <!-- End Google Tag Manager -->
    <!-- Hotjar Tracking Code for www.khadamakum.com -->
    <script>
        (function(h, o, t, j, a, r) {
            h.hj = h.hj || function() {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {
                hjid: 2502644,
                hjsv: 6
            };
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
</head>

<body class="dsdevice" data-map="0">
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div id="website-logo">
                        <a href="index.html" title="موقع خدمكم للعمالة المنزلية في السعودية">موقع خدمكم للعمالة المنزلية
                            في السعودية</a>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="userMenu text-left sm-center">
                        <ul class=" list-inline">
                            <li class="gray-link "><a href="#" title="تسجيل الدخول لحسابك" rel="nofollow"><i
                                        class="fa  fa-lock fa-2x"></i>دخول</a></li>
                            <li class="gray-link "><a href="#" title="حساب جديد" rel="nofollow"><i
                                        class="fa  fa-user-plus fa-2x"></i>تسجيل</a></li>
                            <li class="gray-link ">
                                <a href="#" title="مكاتب تنازل في السعودية"><i
                                        class="fa fa-2x fa-building-o"></i>مكاتب تنازل</a>
                            </li>
                            <li class="noborderleft freeAfter">
                                <a href="#" title="أضف إعلان"
                                    class="btn btn-warning blocksm btn-icon btn-lg showoverFollow"><i
                                        class="fa fa-plus-square"></i> أضف إعلانك </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row text-center mrtb10">
                <form role="form" id="headerSearchForm" method="GET" action="https://khadamakum.com/ar/search/">
                    <div class="col-xs-12 topnav">
                        <div class="row">
                            <div class="col-md-4">
                                <a href="#" rel="nofollow" data-contentid="allAreas"
                                    class="btn btn-social btn-white btn-block btn-lg selectedModal"
                                    title="اختر المدينة">
                                    <i class="fa fa-map-marker"></i> جميع المدن<i
                                        class="fa fa-chevron-down arrowdown"></i></a>
                                <div id="allAreas" class="hidden">
                                    <ul class="list-inline row modalList">
                                        <li class="col-lg-2 col-md-3 col-xs-4  modalIcon">
                                            <a href="index.html" title="جميع المدن">جميع المدن</a>
                                        </li>

                                        @foreach ($cities as $city)
                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                                <a href="#" title="اعلانات في الدمام">{{ $city->name }}</a>
                                            </li>
                                        @endforeach


                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <a href="#" rel="nofollow" data-contentid="allCatogries" data-type="mainCats"
                                    class="btn btn-social btn-white btn-block btn-lg selectedModal"
                                    title="فئات الاعلانات">
                                    <i class="fa fa-th-large"></i> جميع الفئات<i
                                        class="fa fa-chevron-down arrowdown"></i></a>

                                <div id="allCatogries" class="hidden">
                                    <ul class="list-inline row modalList">
                                        <li class="col-lg-2 col-md-3 col-xs-12 col-sm-4 modalIcon">
                                            <a class="main_cat_search" href="index.html" title="جميع الفئات">جميع
                                                الفئات</a>
                                        </li>
                                        @foreach ($categories as $cat)
                                            <li class="col-lg-2 col-md-3 col-xs-12 col-sm-4 modalIcon">
                                                <a class="main_cat_search" href="{{ $cat->name }}"
                                                    title="{{ $cat->name }}"> {{ $cat->name }} </a>
                                            </li>
                                        @endforeach

                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group input-group-lg">
                                    <input type="text" name="query" class="form-control input-sm" placeholder="بحث عن"
                                        value="" />
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-primary btn-sm">بحث</button>
                                    </span>
                                </div>

                            </div>

                        </div>
                    </div>

                </form>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    @foreach ($banners as $banner)


                    <div class="auto-img banner-bg-gray mrb_10">
                        <a href="login/index.html" target="_blank" class="clickedBanner" data-id="82"><img
                                class="img-responsive" src="{{Voyager::image($banner->image)}}"
                                border="0" title="{{$banner->title}}"
                                alt="{{$banner->details}}" /></a>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </header>
    <div class="container   site-body mrt0">
        <div class="row">
            <div class="col-md-12">
                <h1 class="h1_c font1">موقع خدمكم للعمالة المنزلية في السعودية</h1>
                <div class="homeCatogries greenLink">
                    @foreach ($categories as $cat)
                        <a href="#" title="{{ $cat->name }} ">
                            <img src="{{ Voyager::image($cat->image) }}" title="{{ $cat->name }}  "
                                alt="{{ $cat->name }} ">
                            {{ $cat->name }}</a>
                    @endforeach

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="list-inline  thumb_view">

                    @foreach($ads as $ad)
                    <li class="li-thumb col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
                        <div class="card rel">
                            <div class="adtype_tag">
                                <i class="fa fa-tag"></i> 
                                {{$ad->tag->name}}
                            </div>
                            <div class="list-image"><a
                                    href="ad/3613/%d8%b9%d8%a7%d9%85%d9%84%d8%a9-%d9%85%d9%86%d8%b2%d9%84%d9%8a%d8%a9-%d8%af%d9%88%d9%84%d8%a9-%d9%83%d9%8a%d9%86%d9%8a%d8%a7/index.html"
                                    title="عاملة منزلية من دولة كينيا"><img class="img-responsive"
                                        src="{{Voyager::image($ad->image)}}"
                                        title="عاملة منزلية من دولة كينيا" alt="عاملة منزلية من دولة كينيا" /></a></div>
                            <div class="card-body">
                                <a href="ad/3613/%d8%b9%d8%a7%d9%85%d9%84%d8%a9-%d9%85%d9%86%d8%b2%d9%84%d9%8a%d8%a9-%d8%af%d9%88%d9%84%d8%a9-%d9%83%d9%8a%d9%86%d9%8a%d8%a7/index.html"
                                    title="عاملة منزلية من دولة كينيا" class="card-title truncate-text">{{$ad->title}}</a>
                                <div class="card-time"><i class="fa fa-clock-o"></i>    
                                @php($diffInDays = \Carbon\Carbon::parse($ad->created_at)->diffInDays())

                        @php($showDiff = \Carbon\Carbon::parse($ad->created_at)->diffForHumans())

                        @if($diffInDays > 0)

                        @php($showDiff .= ', '.\Carbon\Carbon::parse($ad->created_at)->addDays($diffInDays)->diffInHours().' Hours')

                        @endif

                        {{$showDiff}}
                            </div>
                                <ol class="list-inline card-catogries truncate-text">
                                    <li><i class="fa fa-map-marker gray"></i>
                                        <a href="#" title="{{$ad->city->name}}">{{$ad->city->name}}</a>
                                    </li>
                                    <li><a href="#"
                                            title="استقدام عاملات في السعودية"> {{$ad->category->name}}</a></li>
                                </ol>
                                <div class="fixed_fav_container fixedbottom">
                                    @if($ad->is_fixed==1)
                                    <div class="fixed-ad">إعلان مثبت</div>
                                    @endif
                                    <div class="nat-ad">كينيا</div>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                  
                  
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="moreAdsLink"><a href="search/index.html"><i class="fa fa-arrow-left"></i> اضغط هنا
                        لتصفح المزيد من الإعلانات </a></div>
            </div>
        </div>

        <div class="row" id="bottomContent">
            <div class="col-md-12 text-center hidden-xs">
                <span id="bottomArrowRight"></span>
                <h4>اعلن عن العمالة المنزلية فى <b>السعودية</b> الآن مجانا </h4>
                <span id="bottomArrowLeft"></span>


                <ul class="list-inline bottomCats">
                    @foreach ($categories as $cat)


                    <li>
                        <a href="place-ad/indexff1d.html?catid=24" rel="nofollow">
                            <img src="{{Voyager::image($cat->image)}}"
                                alt=" {{$cat->name}} " title=" {{$cat->name}}  ">
                            <span>
                                {{$cat->name}}   </span></a>
                    </li>
                    @endforeach

                </ul>
            </div>
        </div>
    </div> <!-- end container -->
    <!-- model -->
    <div class="modal fade largeModel" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body">
                    <div id="modalcontents"></div>
                </div>

            </div>
        </div>
    </div>
    <!-- end model -->

    <!-- footer -->
    <footer class="container-fluid ">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 footermobile">
                    <h3 class="footer-title">موقع خدمكم للعمالة المنزلية في السعودية</h3>
                    <ul class="list-inline  footer-links">
                        - <a href="index.html" title="موقع خدمكم للعمالة المنزلية في السعودية">الرئيسية</a></li>
                        <li>- <a href="%d8%aa%d9%86%d8%a7%d8%b2%d9%84-%d8%b9%d8%a7%d9%85%d9%84%d8%a7%d8%aa/index.html"
                                title="اعلانات تنازل عاملات في السعودية">تنازل عاملات</a></li>
                        <li>- <a href="%d8%aa%d8%a3%d8%ac%d9%8a%d8%b1-%d8%b9%d8%a7%d9%85%d9%84%d8%a7%d8%aa/index.html"
                                title="اعلانات نقل كفالة عاملات في السعودية"> نقل كفالة</a></li>
                    </ul>
                </div>

                <div class="col-xs-12 col-md-9 footermobile">
                    <h3 class="footer-title">تصفح أيضا</h3>
                    <ul class="list-inline footer-links">
                        <li><i class="fa fa-angle-left"></i> <a href="contactus/index.html" title="تواصل معنا">أتصل
                                بنا</a></li>
                        <li><i class="fa fa-angle-left"></i> <a href="faqs/index.html" title="الأسئلة المتكررة">الأسئلة
                                المتكررة</a></li>
                        <li><i class="fa fa-angle-left"></i> <a href="site-map/index.html" title="خريطة الموقع">خريطة
                                الموقع</a></li>
                        <li><i class="fa fa-angle-left"></i> <a href="blacklist/index.html"
                                title="القائمة السوداء">القائمة السوداء</a></li>
                        <li><i class="fa fa-angle-left"></i> <a href="about-us/index.html" title="من نحن">من نحن</a>
                        </li>
                        <li><i class="fa fa-angle-left"></i> <a href="privacy/index.html" title="سياسة الخصوصية">سياسة
                                الخصوصية</a></li>
                        <li><i class="fa fa-angle-left"></i> <a href="terms/index.html" title="شروط الأستخدام">شروط
                                الأستخدام</a></li>
                        <li><i class="fa fa-angle-left"></i> <a href="ad-conditions/index.html"
                                title="شروط إضافة الإعلان">شروط إضافة الإعلان</a></li>
                        <li><i class="fa fa-angle-left"></i> <a href="page/26/index.html"
                                title="الكلمات المحضورة">الكلمات المحضورة</a></li>
                    </ul>
                </div>

                <div class="col-xs-12 col-md-3 mrb_10">
                </div>
            </div>

        </div>
        <div class="row rel" id="footer-bottom">
            <div class="col-md-12 text-center">موقع خدمكم © 2021 - 2022</div>
        </div>
    </footer><!-- end footer -->

    <script src="{{ asset('assets/style/default/js/jquery-1.11.1.min.js') }}"></script>
    <script src="{{ asset('assets/style/default/js/plugins.min.js') }}"></script>
    <script type="text/javascript">
        var app = {
            url: 'https://khadamakum.com/ar/',
            ckprefix: 'sm',
            base: '/ar/',
            lang: 'ar',
            root: '/',
            lat: '24.7135517',
            lng: '46.67529569999999',
            currency: 'ريال',
            uo: '0',
            _token: 'aaf3109673f9c546b037965a23df309a',
            sitekey: '6LeR4psaAAAAAIHK6FpjQE87fKdFztvg6RB5l_9y'
        };
    </script>
    <script src="assets/style/default/js/bootstrap.min.js"></script>
    <script src="assets/style/default/js/app.min3f56.js?v=11"></script>
    <script type="text/javascript">
        $(document).ready(fireWhenReady);
    </script>
    <div class="modal fade popupModal" id="popUpHomeBanner" tabindex="-1" role="dialog" aria-labelledby="popModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content rel">
                <div class="modal-header" style="position: absolute; left: 0; top: -11px; z-index: 10;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div id="popModalcontents">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="auto-img banner-bg-gray mrb_10">
                                    <a href="login/index.html" target="_blank" class="clickedBanner" data-id="60"><img
                                            class="img-responsive"
                                            src="{{Voyager::image(setting('site.main-ad'))}}" border="0"
                                            title="مكتب إمداد الدولية استفسار بخصوص إعلانكم"
                                            alt="مكتب إمداد الدولية استفسار بخصوص إعلانكم" /></a>
                                </div>
                                <a class="btn  btn-default btn-block" href="login/index.html"
                                    title="سجل الدخول للمراسلة" rel="nofollow"><i class="fa fa-lock"></i> سجل
                                    الدخول للمراسلة</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P5JMM67" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
</body>

</html>
