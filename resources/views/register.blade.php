 @include("header_auth")

        <div class="container   site-body" >            <div class="row">
                <div class="col-xs-12">
                                                    <div class="auto-img banner-bg-gray mrb_10">
                                    <a href="https://amelaty.com/ar/login/" target="_blank" class="clickedBanner" data-id="82"><img class="img-responsive"  src="https://amelaty.com/upload/banners/sa/ad-image-61bcdf9de5ee63.gif" border="0"  title="مكتب واحة الضياء السلام عليكم، استفسار بخصوص إعلانكم"  alt="مكتب واحة الضياء السلام عليكم، استفسار بخصوص إعلانكم" /></a>                                </div>
                                                                <div class="auto-img banner-bg-gray mrb_10">
                                    <a href="https://amelaty.com/ar/login/" target="_blank" class="clickedBanner" data-id="87"><img class="img-responsive"  src="https://amelaty.com/upload/banners/sa/ad-image-61d8a4e9ea66911.gif" border="0"  title="مكتب السبيعي السلام عليكم، استفسار بخصوص إعلانكم"  alt="مكتب السبيعي السلام عليكم، استفسار بخصوص إعلانكم" /></a>                                </div>
                                                                <div class="auto-img banner-bg-gray mrb_10">
                                    <a href="https://amelaty.com/ar/login/" target="_blank" class="clickedBanner" data-id="61"><img class="img-responsive"  src="https://amelaty.com/upload/banners/sa/ad-image-60c68a4eb5d169.gif" border="0"  title="مكتب إمداد الدولية استفسار بخصوص إعلانكم"  alt="مكتب إمداد الدولية استفسار بخصوص إعلانكم" /></a>                                </div>
                                                </div>
            </div>
            <div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope  itemtype="https://schema.org/ListItem">
                <a itemprop="item" href="https://amelaty.com/ar/" title="موقع عاملتي للعمالة المنزلية في السعودية">
                    <i class="fa fa-home"></i> <span class="visible-lg-inline" itemprop="name">موقع عاملتي للعمالة المنزلية في السعودية</span></a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="active" itemprop="itemListElement" itemscope  itemtype="https://schema.org/ListItem">
                <span itemprop="name">أشتراك جديد</span>
                <meta itemprop="position" content="2" />
            </li>
        </ol>
    </div>
</div>
<div id="register-container">
    <div class="row">

        
        <div class="col-md-12">
            <form role="form" id="register-mobile-form" class="register-mobile-form" method="POST" >
    <div class="row">
        <div class="col-md-6">
            <div class="form-group"> 
                <select name="account_type" id="account_type" class="form-control input-lg">
                    <option value="">اختر نوع الحساب</option>
                    <option value="office" >حساب مكتب استقدام</option>
                    <option value="personal" >حساب شخصي</option>
                </select>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <input type="text" class="form-control input-lg" name="name" autocomplete="off" id="name" placeholder="الإسم" >
            </div>
        </div>


        <div class="col-md-6">
            <div class="form-group fix-feedicon">
                <div class="input-group input-group-lg">
                    <input type="tel"  class="form-control" name="mobile" autocomplete="off"  placeholder="رقم الجوال  مثال (0512345678)">
                    <div class="input-group-addon addon-color"><span id="dailcode">966</span></div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group"> 
                <input type="password" class="form-control input-lg" name="regpassword" autocomplete="off" id="regpassword" placeholder="كلمة المرور">
            </div>
        </div>

        <div id="office_data" class="hidden">
            <div class="col-md-12">                            
                <h3 class="largeFont  mrb2">بيانات المكتب</h3>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <input type="text" class="form-control input-lg" name="storename" id="storename" placeholder="اسم المكتب" >
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group"> 
                    <select name="areaid" class="form-control input-lg">
                        <option value="">المدينة</option>
                                                        <option value="290" >جميع المدن</option>
                                                                <option value="288" >الدمام</option>
                                                                <option value="268" >الرياض</option>
                                                                <option value="269" >جده</option>
                                                                <option value="281" >مكه</option>
                                                                <option value="275" >ينبع</option>
                                                                <option value="274" >حفر الباطن</option>
                                                                <option value="273" >المدينة</option>
                                                                <option value="280" >الطائف</option>
                                                                <option value="276" >تبوك</option>
                                                                <option value="271" >القصيم</option>
                                                                <option value="270" >حائل</option>
                                                                <option value="272" >أبها</option>
                                                                <option value="283" >الباحه</option>
                                                                <option value="277" >جيزان</option>
                                                                <option value="278" >نجران</option>
                                                                <option value="284" >الجوف</option>
                                                                <option value="279" >عرعر</option>
                                                                <option value="286" >رفحاء</option>
                                                                <option value="287" >عسير</option>
                                                    </select>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <textarea  rows="6" cols="95" name="details" id="details" class="form-control input-lg"  placeholder="نبذة عن المكتب" ></textarea>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-image"></i> لوجو / شعار</div>
                    <ul id="customUpload" class="list-inline clearfix" data-up="false" data-id="0" data-multi="true"  data-mf="1" data-watermark="0" >
                        <li id="uploadBtn">
                            <div class="icon">
                                <i class="fa fa-3x fa-upload"></i>
                                <span>تحميل صورة</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
                            <div class="col-md-12">
                    <div class="form-group"> 
                        <div class="mrb_10 bold font2">اختر مجال نشاطك :</div>
                                                    <label class="checkbox-inline padr0">
                                <input type="checkbox"  name="catid[]" value="استقدام-عاملات">
                                استقدام عاملات                            </label>
                                                        <label class="checkbox-inline padr0">
                                <input type="checkbox"  name="catid[]" value="تنازل-عاملات">
                                تنازل عاملات                            </label>
                                                        <label class="checkbox-inline padr0">
                                <input type="checkbox"  name="catid[]" value="تأجير-عاملات">
                                تأجير عاملات                            </label>
                                                </div>
                </div>
                        </div>

                <div class="col-md-12">
            <button type="submit" class="btn btn-lg btn-success blocksm" id="registerMobileBtn">ارسال</button>
            <input type="hidden" name="_token" value="9acb47460f98cc5655321168ddf6ec32" />
        </div>

    </div>
</form>





        </div>
    </div>
</div>

</div> <!-- end container -->
<!-- model -->
<div class="modal fade largeModel" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body" >
                <div id="modalcontents"></div>
            </div>

        </div>
    </div>
</div>
<!-- end model -->

<!-- footer -->
<footer class="container-fluid ">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 footermobile">
                <h3 class="footer-title">موقع عاملتي للعمالة المنزلية في السعودية</h3>
                <ul class="list-inline  footer-links">
                    - <a href="https://amelaty.com/ar/" title="موقع عاملتي للعمالة المنزلية في السعودية">الرئيسية</a></li>
                                                <li>- <a href="https://amelaty.com/ar/استقدام-عاملات/" title="اعلانات استقدام عاملات في السعودية">استقدام عاملات</a></li>
                                                        <li>- <a href="https://amelaty.com/ar/تنازل-عاملات/" title="اعلانات تنازل عاملات في السعودية">تنازل عاملات</a></li>
                                                        <li>- <a href="https://amelaty.com/ar/تأجير-عاملات/" title="اعلانات تأجير عاملات في السعودية">تأجير عاملات</a></li>
                                            </ul>
            </div>

            <div class="col-xs-12 col-md-9 footermobile">
                <h3 class="footer-title">تصفح أيضا</h3>
                <ul class="list-inline footer-links">
                    <li><i class="fa fa-angle-left"></i> <a  href="https://amelaty.com/ar/contactus/" title="تواصل معنا">أتصل بنا</a></li>
                    <li><i class="fa fa-angle-left"></i> <a href="https://amelaty.com/ar/faqs/" title="الأسئلة المتكررة">الأسئلة المتكررة</a></li>
                    <li><i class="fa fa-angle-left"></i> <a href="https://amelaty.com/ar/site-map/" title="خريطة الموقع">خريطة الموقع</a></li>
                    <li><i class="fa fa-angle-left"></i> <a href="https://amelaty.com/ar/blacklist/" title="القائمة السوداء">القائمة السوداء</a></li>
                                                    <li><i class="fa fa-angle-left"></i> <a href="https://amelaty.com/ar/about-us/" title="من نحن">من نحن</a></li>
                                                                <li><i class="fa fa-angle-left"></i> <a href="https://amelaty.com/ar/privacy/" title="سياسة الخصوصية">سياسة الخصوصية</a></li>
                                                                <li><i class="fa fa-angle-left"></i> <a href="https://amelaty.com/ar/terms/" title="شروط الأستخدام">شروط الأستخدام</a></li>
                                                                <li><i class="fa fa-angle-left"></i> <a href="https://amelaty.com/ar/ad-conditions/" title="شروط إضافة الإعلان">شروط إضافة الإعلان</a></li>
                                                                <li><i class="fa fa-angle-left"></i> <a href="https://amelaty.com/ar/page/26/" title="الكلمات المحضورة">الكلمات المحضورة</a></li>
                                                </ul>
            </div>

            <div class="col-xs-12 col-md-3 mrb_10">
                            </div>
        </div>

    </div> 
    <div class="row rel"  id="footer-bottom">
                <div class="col-md-12 text-center">موقع عاملتي © 2021 - 2022</div>
    </div>
</footer><!-- end footer -->

<script src="https://amelaty.com/style/default/js/jquery-1.11.1.min.js"></script>
<script src="https://amelaty.com/style/default/js/plugins.min.js"></script>
<script type="text/javascript">
    var app = {
        url: 'https://amelaty.com/ar/',
        ckprefix: 'sm',
        base: '/ar/',
        lang: 'ar',
        root: '/',
        lat: '24.7135517',
        lng: '46.67529569999999',
        currency: 'ريال',
        uo: '0',
        _token: '9acb47460f98cc5655321168ddf6ec32',
        sitekey: '6LeR4psaAAAAAIHK6FpjQE87fKdFztvg6RB5l_9y'
    };
</script>
<script src="https://amelaty.com/style/default/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://amelaty.com/style/default/js/plupload/plupload.full.min.js"></script>
    <script src="https://amelaty.com/style/default/js/app.min.js?v=11"></script>
    <script type="text/javascript" src="https://amelaty.com/style/default/js/formValidation/formValidation.min.js?v=11"></script>
    <script type="text/javascript" src="https://amelaty.com/style/default/js/formValidation/framework/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://amelaty.com/style/default/js/validate.min.js?v=11"></script>
    <script type="text/javascript">
    $(document).ready(fireWhenReady);
</script>
        <div class="modal fade popupModal" id="popUpHomeBanner" tabindex="-1" role="dialog" aria-labelledby="popModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content rel">
                    <div class="modal-header" style="position: absolute; left: 0; top: -11px; z-index: 10;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body" >
                        <div id="popModalcontents">
                            <div class="row">
                                <div class="col-xs-12">
                                                                            <div class="auto-img banner-bg-gray mrb_10">
                                            <a href="https://amelaty.com/ar/login/" target="_blank" class="clickedBanner" data-id="89"><img class="img-responsive"  src="https://amelaty.com/upload/banners/sa/ad-image-61d8a868ce63412.gif" border="0"  title="مكتب السبيعي السلام عليكم، استفسار بخصوص إعلانكم"  alt="مكتب السبيعي السلام عليكم، استفسار بخصوص إعلانكم" /></a>                                        </div>
                                                                                    <a class="btn  btn-default btn-block" href="https://amelaty.com/ar/login/"  title="سجل الدخول للمراسلة" rel="nofollow"><i class="fa fa-lock"></i> سجل الدخول للمراسلة</a>
                                                                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P5JMM67"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) --></body>
</html>

 
       <script type="text/javascript">
        $(document).ready(function(){
            
        });
    </script>
 