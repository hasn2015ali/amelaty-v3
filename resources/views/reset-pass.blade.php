@include("header")
<body class="dsdevice" data-map="0">
    <header>
        <div class="container">
<div class="row" >
    <div class="col-md-4">
        <div id="website-logo">
            <a href="https://amelaty.com/ar/" title="موقع عاملتي للعمالة المنزلية في السعودية">موقع عاملتي للعمالة المنزلية في السعودية</a>
        </div>
    </div>

    <div class="col-md-8">
        <div class="userMenu text-left sm-center">
            <ul class=" list-inline">
                                        <li class="gray-link " ><a  href="https://amelaty.com/ar/login/" title="تسجيل الدخول لحسابك" rel="nofollow"><i class="fa  fa-lock fa-2x"></i>دخول</a></li>
                    <li class="gray-link " ><a  href="https://amelaty.com/ar/register/" title="حساب جديد" rel="nofollow"><i class="fa  fa-user-plus fa-2x"></i>تسجيل</a></li>
                                            <li class="gray-link ">
                    <a  href="https://amelaty.com/ar/مكاتب-الاستقدام/" title="مكاتب الاستقدام في السعودية"><i class="fa fa-2x fa-building-o"></i>مكاتب الاستقدام</a>
                </li>
                <li class="noborderleft freeAfter">
                    <a href="https://amelaty.com/ar/place-ad/" title="أضف إعلان" class="btn btn-warning blocksm btn-icon btn-lg showoverFollow"><i class="fa fa-plus-square"></i> أضف إعلانك </a>
                </li>
            </ul>
        </div>
    </div>
</div>
</div>
        <div class="container">
            <div class="row text-center mrtb10">
<form role="form" id="headerSearchForm" method="GET" action="https://amelaty.com/ar/search/" >
    <div class="col-xs-12 topnav" >
        <div class="row">
                                <div class="col-md-4">
                    <a  href="#" rel="nofollow" data-contentid="allAreas"  class="btn btn-social btn-white btn-block btn-lg selectedModal" title="اختر المدينة">
                        <i class="fa fa-map-marker"></i> جميع المدن<i class="fa fa-chevron-down arrowdown"></i></a>
                    <div id="allAreas" class="hidden">
                        <ul class="list-inline row modalList">
                            <li class="col-lg-2 col-md-3 col-xs-4  modalIcon">
                                <a href="https://amelaty.com/ar/" title="جميع المدن">جميع المدن</a>
                            </li>
                                                                    <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/جميع-المدن/" title="اعلانات في جميع المدن">جميع المدن</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/الدمام/" title="اعلانات في الدمام">الدمام</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/الرياض/" title="اعلانات في الرياض">الرياض</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/جده/" title="اعلانات في جده">جده</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/مكه/" title="اعلانات في مكه">مكه</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/ينبع/" title="اعلانات في ينبع">ينبع</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/حفر-الباطن/" title="اعلانات في حفر الباطن">حفر الباطن</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/المدينة/" title="اعلانات في المدينة">المدينة</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/الطائف/" title="اعلانات في الطائف">الطائف</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/تبوك/" title="اعلانات في تبوك">تبوك</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/القصيم/" title="اعلانات في القصيم">القصيم</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/حائل/" title="اعلانات في حائل">حائل</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/أبها/" title="اعلانات في أبها">أبها</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/الباحه/" title="اعلانات في الباحه">الباحه</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/جيزان/" title="اعلانات في جيزان">جيزان</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/نجران/" title="اعلانات في نجران">نجران</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/الجوف/" title="اعلانات في الجوف">الجوف</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/عرعر/" title="اعلانات في عرعر">عرعر</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/رفحاء/" title="اعلانات في رفحاء">رفحاء</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-4 modalIcon">
                                        <a href="https://amelaty.com/ar/city/عسير/" title="اعلانات في عسير">عسير</a>
                                    </li>
                                                                </ul>
                    </div>
                                        </div>
                                    <div class="col-md-4">
                    <a  href="#" rel="nofollow" data-contentid="allCatogries" data-type="mainCats"  class="btn btn-social btn-white btn-block btn-lg selectedModal" title="فئات الاعلانات">
                        <i class="fa fa-th-large"></i> جميع الفئات<i class="fa fa-chevron-down arrowdown"></i></a>

                    <div id="allCatogries" class="hidden">
                        <ul class="list-inline row modalList">
                            <li class="col-lg-2 col-md-3 col-xs-12 col-sm-4 modalIcon">
                                <a class="main_cat_search" href="https://amelaty.com/ar/" title="جميع الفئات">جميع الفئات</a>
                            </li>
                                                                    <li class="col-lg-2 col-md-3 col-xs-12 col-sm-4 modalIcon">
                                        <a class="main_cat_search" href="https://amelaty.com/ar/استقدام-عاملات/" title="استقدام عاملات في السعودية">استقدام عاملات</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-12 col-sm-4 modalIcon">
                                        <a class="main_cat_search" href="https://amelaty.com/ar/تنازل-عاملات/" title="تنازل عاملات في السعودية">تنازل عاملات</a>
                                    </li>
                                                                            <li class="col-lg-2 col-md-3 col-xs-12 col-sm-4 modalIcon">
                                        <a class="main_cat_search" href="https://amelaty.com/ar/تأجير-عاملات/" title="تأجير عاملات في السعودية">تأجير عاملات</a>
                                    </li>
                                                                </ul>
                    </div>
                </div>
                                <div class="col-md-4">
                <div class="input-group input-group-lg">
                    <input type="text" name="query" class="form-control input-sm" placeholder="بحث عن" value="" />
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-primary btn-sm">بحث</button>
                    </span>
                </div>

            </div>

        </div>
    </div>

</form>
</div>

        <div class="row">
            <div class="col-xs-12">
                                                <div class="auto-img banner-bg-gray mrb_10">
                                <a href="https://amelaty.com/ar/login/" target="_blank" class="clickedBanner" data-id="82"><img class="img-responsive"  src="https://amelaty.com/upload/banners/sa/ad-image-61bcdf9de5ee63.gif" border="0"  title="مكتب واحة الضياء السلام عليكم، استفسار بخصوص إعلانكم"  alt="مكتب واحة الضياء السلام عليكم، استفسار بخصوص إعلانكم" /></a>                                </div>
                                                            <div class="auto-img banner-bg-gray mrb_10">
                                <a href="https://amelaty.com/ar/login/" target="_blank" class="clickedBanner" data-id="87"><img class="img-responsive"  src="https://amelaty.com/upload/banners/sa/ad-image-61d8a4e9ea66911.gif" border="0"  title="مكتب السبيعي السلام عليكم، استفسار بخصوص إعلانكم"  alt="مكتب السبيعي السلام عليكم، استفسار بخصوص إعلانكم" /></a>                                </div>
                                                            <div class="auto-img banner-bg-gray mrb_10">
                                <a href="https://amelaty.com/ar/login/" target="_blank" class="clickedBanner" data-id="61"><img class="img-responsive"  src="https://amelaty.com/upload/banners/sa/ad-image-60c68a4eb5d169.gif" border="0"  title="مكتب إمداد الدولية استفسار بخصوص إعلانكم"  alt="مكتب إمداد الدولية استفسار بخصوص إعلانكم" /></a>                                </div>
                                            </div>
        </div>
                    </div>
    </header>
    <div class="container   site-body" >
<div class="row">

<div class="col-md-12">
    <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope  itemtype="https://schema.org/ListItem">
            <a itemprop="item" href="https://amelaty.com/ar/" title="موقع عاملتي للعمالة المنزلية في السعودية"><i class="fa fa-home"></i> <span class="visible-lg-inline" itemprop="name">موقع عاملتي للعمالة المنزلية في السعودية</span></a>
            <meta itemprop="position" content="1" />
        </li>
        <li class="active"  itemprop="itemListElement" itemscope  itemtype="https://schema.org/ListItem">
            <span itemprop="name">أستعادة كلمة المرور</span>
            <meta itemprop="position" content="2" />
        </li>
    </ol>
</div>

<div class="col-md-12">
    <h3 class="largeFont  mrb2 sm-center">أستعادة كلمة المرور لحسابك : </h3>
</div>

<div class="col-md-12">
                <form role="form" id="bysms-form" class="bysms-form" method="POST"  autocomplete="off">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group fix-feedicon">
                        <div class="input-group input-group-lg">
                            <input type="tel"  class="form-control" name="mobile" id="mobile" placeholder="رقم الجوال  مثال (0512345678)">
                            <div class="input-group-addon addon-color"><span id="dailcode">966</span></div>
                        </div>
                    </div>
                </div>

                                    <div class="col-md-12">
                    <button type="submit" class="btn btn-lg btn-success blocksm" id="bysmsBtn">ارسال</button>
                    <input type="hidden" name="_token" value="aaf3109673f9c546b037965a23df309a" />                    </div>

            </div>
        </form>
            </div>
</div>
</div> <!-- end container -->
<!-- model -->
<div class="modal fade largeModel" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"></h4>
        </div>
        <div class="modal-body" >
            <div id="modalcontents"></div>
        </div>

    </div>
</div>
</div>
<!-- end model -->


@include('footer')
