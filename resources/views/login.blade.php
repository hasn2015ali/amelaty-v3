@include("header")

<div class="container   site-body">
    <div class="row">
        <div class="col-xs-12">
            @foreach ($banners as $banner)


            <div class="auto-img banner-bg-gray mrb_10">
                <a href="{{url('login)}}" target="_blank" class="clickedBanner" data-id="82"><img class="img-responsive"
                 src="{{Voyager::image($banner->image)}}" border="0" title="{{$banner->title}}" alt="{{$banner->details}}" /></a>
            </div>
            @endforeach
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a itemprop="item" href="../index.html" title="موقع عاملتي للعمالة المنزلية في السعودية">
                        <i class="fa fa-home"></i> <span class="visible-lg-inline" itemprop="name">موقع عاملتي للعمالة المنزلية في السعودية</span></a>
                    <meta itemprop="position" content="1" />
                </li>
                <li class="active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <span itemprop="name">تسجيل الدخول</span>
                    <meta itemprop="position" content="2" />
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form role="form" id="login-form" class="login-form" method="POST" data-modal="0">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group fix-feedicon">
                            <div class="input-group input-group-lg">
                                <input type="tel" class="form-control" name="mobile" autocomplete="off" placeholder="رقم الجوال  مثال (0512345678)">
                                <div class="input-group-addon addon-color"><span id="dailcodeLogin">966</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="password" class="form-control input-lg" name="regpassword" autocomplete="off" id="regpassword" placeholder="كلمة المرور">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <button type="submit" class="btn btn-lg btn-success blocksm" id="loginBtn">دخول</button>
                        <input type="hidden" name="_token" value="aaf3109673f9c546b037965a23df309a" />
                    </div>

                    <div class="col-md-6 text-left sm-center">
                        <a href="{{url('restore-password-form')}}" rel="nofollow"><i class="fa fa-question-circle"></i> نسيت كلمة المرور ؟</a>
                        <p style="display: block; margin-top: 10px; margin-bottom: 10px;">
                            اذا لم يكن لديك حساب يمكنك <a class="bold" href="../register/index.html" title="تسجيل أشتراك جديد">التسجيل</a>
                        </p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div> <!-- end container -->
<!-- model -->
<div class="modal fade largeModel" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div id="modalcontents"></div>
            </div>

        </div>
    </div>
</div>
<!-- end model -->


@include('footer')