<!DOCTYPE html>
<html lang="ar" prefix="og: http://ogp.me/ns#">

<head>
    <meta charset="utf-8">
    <title>موقع خدمكم للعمالة المنزلية في السعودية</title>
    <meta name="description"
        content="خدمكم للعمالة المنزلية في السعودية استقدام وتنازل خادمات تأجير بالساعة باليوم بالشهر من جميع الدول الفلبين المغرب الهند بنجلاديش أوغندا.">
    <meta name="Keywords"
        content="خدمكم,عمالة منزلية في السعودية ,خادمات للتنازل, خادمات بالساعة,خادمات بالشهر, استقدام خادمات, تأجير خادمات, خادمات للتنازل, عاملات بالساعة,عاملات بالشهر,خادمات فلبينيات, خادمات مغربيات, خادمات أوغندا, خادمات بنجلاديش, شركات تأجير خادمات بالشهر">
    <meta http-equiv="content-language" content="ar-sa">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-dns-prefetch-control" content="on">
    <meta name="ROBOTS" content="INDEX, FOLLOW">
    <link rel="dns-prefetch" href="http://www.googletagmanager.com/">
    <link rel="dns-prefetch" href="http://www.google-analytics.com/">
    <link rel="dns-prefetch" href="http://ssl.google-analytics.com/">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="موقع خدمكم للعمالة المنزلية في السعودية" />
    <meta property="og:site_name" content="موقع خدمكم للعمالة المنزلية في السعودية" />
    <meta property="og:description"
        content="خدمكم للعمالة المنزلية في السعودية استقدام وتنازل خادمات تأجير بالساعة باليوم بالشهر من جميع الدول الفلبين المغرب الهند بنجلاديش أوغندا." />
    <meta property="og:keyword"
        content="خدمكم,عمالة منزلية في السعودية ,خادمات للتنازل, خادمات بالساعة,خادمات بالشهر, استقدام خادمات, تأجير خادمات, خادمات للتنازل, عاملات بالساعة,عاملات بالشهر,خادمات فلبينيات, خادمات مغربيات, خادمات أوغندا, خادمات بنجلاديش, شركات تأجير خادمات بالشهر" />
    <meta name="REVISIT-AFTER" content="1 DAYS">
    <meta name="RATING" content="GENERAL">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <meta name="author" content="websecretsgroup.com">
    <link rel="manifest" href="manifest.json">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="white">
    <meta name="apple-mobile-web-app-title" content="خدمكم">
    <meta name="application-name" content="خدمكم">
    <link rel="apple-touch-icon" href="assets/icon/icon-57x57.png" sizes="57x57">
    <link rel="apple-touch-icon" href="assets/icon/icon-180x180.png" sizes="180x180">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/icon/icon-32x32.png">
    <link rel="icon" type="image/png" sizes="194x194" href="assets/icon/icon-194x194.png">
    <link rel="icon" type="image/png" sizes="192x192" href="assets/icon/icon-192x192.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/icon/icon-16x16.png">
    <link rel="mask-icon" href="assets/icon/safari-pinned-tab.svg" color="#4091d2">
    <meta name="msapplication-TileImage" content="assets/icon/icon-144x144.png">
    <meta name="msapplication-TileColor" content="#fff">
    <meta name="msapplication-config" content="assets/icon/browserconfig.xml">
    <meta name="theme-color" content="#4091d2">
    <link rel="shortcut icon" href="assets/icon/favicon.ico">
    <link rel="stylesheet" href="{{ asset('assets/style/default/css/bootstrap-ar.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets/style/default/fonts/font-awesome-4.4.0/css/font-awesome.min.css') }}">
    <link href="{{ asset('assets/style/default/css/styles.min3f56.css') }}?v=11" rel="stylesheet">

    <style>
        #website-logo a {
            background: none;
        }

    </style>
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-P5JMM67');
    </script>
    <!-- End Google Tag Manager -->
    <!-- Hotjar Tracking Code for www.khadamakum.com -->
    <script>
        (function(h, o, t, j, a, r) {
            h.hj = h.hj || function() {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {
                hjid: 2502644,
                hjsv: 6
            };
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
</head>

<body class="dsdevice" data-map="0">
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div id="website-logo">
                        <a href="index.html" title="موقع خدمكم للعمالة المنزلية في السعودية">موقع خدمكم للعمالة المنزلية
                            في السعودية</a>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="userMenu text-left sm-center">
                        <ul class=" list-inline">
                            <li class="gray-link "><a href="#" title="تسجيل الدخول لحسابك" rel="nofollow"><i
                                        class="fa  fa-lock fa-2x"></i>دخول</a></li>
                            <li class="gray-link "><a href="#" title="حساب جديد" rel="nofollow"><i
                                        class="fa  fa-user-plus fa-2x"></i>تسجيل</a></li>
                            <li class="gray-link ">
                                <a href="#" title="مكاتب الاستقدام في السعودية"><i
                                        class="fa fa-2x fa-building-o"></i>مكاتب الاستقدام</a>
                            </li>
                            <li class="noborderleft freeAfter">
                                <a href="#" title="أضف إعلان"
                                    class="btn btn-warning blocksm btn-icon btn-lg showoverFollow"><i
                                        class="fa fa-plus-square"></i> أضف إعلانك </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            

            <div class="row">
                <div class="col-xs-12">
                    @foreach ($banners as $banner)


                    <div class="auto-img banner-bg-gray mrb_10">
                        <a href="login/index.html" target="_blank" class="clickedBanner" data-id="82"><img
                                class="img-responsive" src="{{Voyager::image($banner->image)}}"
                                border="0" title="{{$banner->title}}"
                                alt="{{$banner->details}}" /></a>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </header>
