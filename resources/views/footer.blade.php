
    <!-- footer -->
    <footer class="container-fluid ">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 footermobile">
                    <h3 class="footer-title">موقع خدمكم للعمالة المنزلية في السعودية</h3>
                    <ul class="list-inline  footer-links">
                        - <a href="index.html" title="موقع خدمكم للعمالة المنزلية في السعودية">الرئيسية</a></li>
                        <li>- <a href="%d8%aa%d9%86%d8%a7%d8%b2%d9%84-%d8%b9%d8%a7%d9%85%d9%84%d8%a7%d8%aa/index.html"
                                title="اعلانات تنازل عاملات في السعودية">تنازل عاملات</a></li>
                        <li>- <a href="%d8%aa%d8%a3%d8%ac%d9%8a%d8%b1-%d8%b9%d8%a7%d9%85%d9%84%d8%a7%d8%aa/index.html"
                                title="اعلانات نقل كفالة عاملات في السعودية"> نقل كفالة</a></li>
                    </ul>
                </div>

                <div class="col-xs-12 col-md-9 footermobile">
                    <h3 class="footer-title">تصفح أيضا</h3>
                    <ul class="list-inline footer-links">
                        <li><i class="fa fa-angle-left"></i> <a href="contactus/index.html" title="تواصل معنا">أتصل
                                بنا</a></li>
                        <li><i class="fa fa-angle-left"></i> <a href="faqs/index.html" title="الأسئلة المتكررة">الأسئلة
                                المتكررة</a></li>
                        <li><i class="fa fa-angle-left"></i> <a href="site-map/index.html" title="خريطة الموقع">خريطة
                                الموقع</a></li>
                        <li><i class="fa fa-angle-left"></i> <a href="blacklist/index.html"
                                title="القائمة السوداء">القائمة السوداء</a></li>
                        <li><i class="fa fa-angle-left"></i> <a href="about-us/index.html" title="من نحن">من نحن</a>
                        </li>
                        <li><i class="fa fa-angle-left"></i> <a href="privacy/index.html" title="سياسة الخصوصية">سياسة
                                الخصوصية</a></li>
                        <li><i class="fa fa-angle-left"></i> <a href="terms/index.html" title="شروط الأستخدام">شروط
                                الأستخدام</a></li>
                        <li><i class="fa fa-angle-left"></i> <a href="ad-conditions/index.html"
                                title="شروط إضافة الإعلان">شروط إضافة الإعلان</a></li>
                        <li><i class="fa fa-angle-left"></i> <a href="page/26/index.html"
                                title="الكلمات المحضورة">الكلمات المحضورة</a></li>
                    </ul>
                </div>

                <div class="col-xs-12 col-md-3 mrb_10">
                </div>
            </div>

        </div>
        <div class="row rel" id="footer-bottom">
            <div class="col-md-12 text-center">موقع خدمكم © 2021 - 2022</div>
        </div>
    </footer><!-- end footer -->

    <script src="{{ asset('assets/style/default/js/jquery-1.11.1.min.js') }}"></script>
    <script src="{{ asset('assets/style/default/js/plugins.min.js') }}"></script>
    <script type="text/javascript">
        var app = {
            url: 'https://khadamakum.com/ar/',
            ckprefix: 'sm',
            base: '/ar/',
            lang: 'ar',
            root: '/',
            lat: '24.7135517',
            lng: '46.67529569999999',
            currency: 'ريال',
            uo: '0',
            _token: 'aaf3109673f9c546b037965a23df309a',
            sitekey: '6LeR4psaAAAAAIHK6FpjQE87fKdFztvg6RB5l_9y'
        };
    </script>
    <script src="assets/style/default/js/bootstrap.min.js"></script>
    <script src="assets/style/default/js/app.min3f56.js?v=11"></script>
    <script type="text/javascript">
        $(document).ready(fireWhenReady);
    </script>
    <div class="modal fade popupModal" id="popUpHomeBanner" tabindex="-1" role="dialog" aria-labelledby="popModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content rel">
                <div class="modal-header" style="position: absolute; left: 0; top: -11px; z-index: 10;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div id="popModalcontents">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="auto-img banner-bg-gray mrb_10">
                                    <a href="login/index.html" target="_blank" class="clickedBanner" data-id="60"><img
                                            class="img-responsive"
                                            src="assets/upload/banners/sa/ad-image-60c68de00cadd13.gif" border="0"
                                            title="مكتب إمداد الدولية استفسار بخصوص إعلانكم"
                                            alt="مكتب إمداد الدولية استفسار بخصوص إعلانكم" /></a>
                                </div>
                                <a class="btn  btn-default btn-block" href="login/index.html"
                                    title="سجل الدخول للمراسلة" rel="nofollow"><i class="fa fa-lock"></i> سجل
                                    الدخول للمراسلة</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P5JMM67" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
</body>

</html>
