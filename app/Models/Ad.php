<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    use HasFactory;
    public function category(){
        return $this->belongsTo(AdCategory::class,'cat_id','id');
    }
    public function city(){
        return $this->belongsTo(City::class,'city_id','id');
    }
    public function tag(){
        return $this->belongsTo(AdTag::class,'tag_id','id');
    }
}
