<?php

namespace App\Http\Controllers;

use App\Models\AdCategory;
use App\Models\Banner;
use App\Models\City;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //
     public function registerForm()
    {
        $work_field= AdCategory::get();
        $cities = City::get();
        $banners = Banner::get();

        return view("register", compact('work_field', 'cities','banners'));
    }
    public function loginForm(){
        $work_field= AdCategory::get();
        $cities = City::get();
        $categories = AdCategory::get();
        $banners = Banner::get();


        return view("login", compact('work_field', 'cities','categories','banners'));
    }
    public function restorePasswordForm(){
        return view("login");
    }
}
