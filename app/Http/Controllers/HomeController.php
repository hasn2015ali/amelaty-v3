<?php

namespace App\Http\Controllers;

use App\Models\AdCategory;
use App\Models\Banner;
use App\Models\City;
use App\Models\Ad;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function index(){
        $cities = City::get();
        $categories = AdCategory::get();
        $banners = Banner::get();
        $ads = Ad::with('category','city','tag')->paginate(20);

        return view("welcome", compact('cities','categories','banners','ads'));
    }
}
